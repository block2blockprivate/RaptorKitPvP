package me.jacobruby.kitpvp.mechanics.mapgeneration;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import com.sk89q.worldedit.world.DataException;
import io.nv.bukkit.CleanroomGenerator.CleanroomChunkGenerator;
import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.listeners.KitSelectionListener;
import me.jacobruby.kitpvp.util.UtilBlock;
import me.jacobruby.kitpvp.util.UtilLocation;
import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static org.bukkit.Material.*;
import static org.bukkit.block.Biome.*;
import static org.bukkit.block.BlockFace.*;

@SuppressWarnings("deprecation")
public class MapManager {

    private static Kitpvp k = Kitpvp.get();

    private String worldName;
    private File schem;

    public List<Block> supplyDropList = new ArrayList<>();
    public Set<Block> pads = new HashSet<>();
    public List<Block> borders = new ArrayList<>();

    public Location spawn = null;

    public int spawnYLevel = 150;

    public Block min = null;
    public Block max = null;

    public MapManager(String mapName, String schematic) throws FileNotFoundException {
        worldName = mapName;
        schem = new File("plugins/RaptorKitPvP/schematics/"+schematic+".schematic");
        if (!schem.exists()) {
            throw new FileNotFoundException("file "+schematic+" was not found in plugins\\RaptorKitPvP\\schematics");
        }
    }

    public World load() {
        WorldCreator worldCreator;
        worldCreator = new WorldCreator(worldName).generator(new CleanroomChunkGenerator(".")).generateStructures(false);
        World world = worldCreator.createWorld();
        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("mobGriefing", "false");
        world.setGameRuleValue("doMobLoot", "false");
        world.setGameRuleValue("doEntityDrops", "false");
        world.setGameRuleValue("doTileDrops", "false");
        world.setGameRuleValue("doFireTick", "false");
        world.setGameRuleValue("keepInventory", "true");
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setTime(6000);
        try {
            EditSession es = k.we.getWorldEdit().getEditSessionFactory().getEditSession(new BukkitWorld(world), 1000000000);
            new MapGenerator(MCEditSchematicFormat.getFormat(schem).load(schem), 0, es).runTaskLater(k, 100);
            return world;
        } catch (DataException | IOException e) {
            k.l.s(e.getMessage());
            e.printStackTrace();
            for (StackTraceElement st : e.getStackTrace()) {
                k.broadcast(st.toString());
            }
            k.broadcast(e.getMessage());
            return null;
        }
    }

    public void generateDataPoints(int level) {
        if (level<0)
            return;
        int length = 0;
        int width = 0;
        try {
            length = MCEditSchematicFormat.getFormat(schem).load(schem).getLength();
            width = MCEditSchematicFormat.getFormat(schem).load(schem).getWidth();
        } catch (IOException | com.sk89q.worldedit.data.DataException e) {
            e.printStackTrace();
            k.broadcast(e.getMessage());
        }
        try {
            List<Block> blocks;
            try {
                blocks = UtilBlock.allBlocksInside(UtilBlock.blockAt(UtilLocation.defaultWorld(), 0, level, 0),
                        UtilBlock.blockAt(UtilLocation.defaultWorld(), length + 1, level, width + 1));
            } catch (Throwable t) {
                k.broadcast(t.getMessage());
                return;
            }
            for (Block b : blocks) {
                if (b == null || b.getType() != WOOL)
                    continue;
                if (!UtilBlock.isDataPoint(b))
                    continue;
                k.broadcast(b.getData() + " at " + b.getX() + ", " + b.getY() + ", " + b.getZ() + " in world " + b.getWorld().getName());
                switch (b.getData()) {
                    case 0: {
                        b.getRelative(UP).setType(AIR);
                        b.setType(AIR);
                        if (min == null)
                            min = b;
                        else max = b;
                        break;
                    }
                    case 3: {
                        b.getRelative(UP).setType(AIR);
                        Sign c = (Sign) b.getRelative(NORTH).getState();
                        float yaw = Float.parseFloat(c.getLine(0));
                        b.setType(AIR);
                        Location loc = UtilBlock.toLocationMiddle(b);
                        loc.setYaw(yaw);
                        spawn = loc;
                        break;
                    }
                    case 4: {
                        b.getRelative(UP).setType(AIR);
                        b.setType(AIR);
                        Block c = b.getRelative(DOWN);
                        c.setType(STAINED_GLASS);
                        c.getRelative(DOWN).setType(BEACON);
                        Block d = c.getRelative(DOWN).getRelative(DOWN);
                        Block[] e = {d, d.getRelative(NORTH), d.getRelative(NORTH_EAST), d.getRelative(EAST),
                                d.getRelative(SOUTH_EAST), d.getRelative(SOUTH), d.getRelative(SOUTH_WEST), d.getRelative(WEST),
                                d.getRelative(NORTH_WEST)};
                        for (Block f : e) {
                            f.setType(IRON_BLOCK);
                        }
                        e[1].setType(AIR);
                        supplyDropList.add(b);
                        break;
                    }
                    case 5: {
                        b.getRelative(UP).setType(AIR);
                        b.setType(SLIME_BLOCK);
                        pads.add(b);
                        break;
                    }
                    case 7: {
                        b.getRelative(UP).setType(AIR);
                        b.setType(AIR);
                        spawnYLevel = b.getLocation().getBlockY();
                        break;
                    }
                    case 12: {
                        b.getRelative(UP).setType(AIR);
                        Sign c = (Sign) b.getRelative(NORTH).getState();
                        float yaw = Float.parseFloat(c.getLine(0));
                        b.setType(AIR);
                        Location loc = UtilBlock.toLocation(b);
                        loc.setYaw(yaw);
                        KitSelectionListener.npcs.put((Villager) b.getWorld().spawnEntity(UtilBlock.toLocation(b), EntityType.VILLAGER), loc);
                        break;
                    }
                    case 14: {
                        b.getRelative(UP).setType(AIR);
                        b.setType(AIR);
                        borders.add(b);
                        break;
                    }
                }
            }
        } catch (Throwable t) {
            for (StackTraceElement st : t.getStackTrace()) {
                k.broadcast(st.toString());
            }
            k.broadcast(" \n" + t.getMessage());
        }
    }

    public class DataGenerator extends BukkitRunnable {

        private CuboidClipboard schem;
        private int level = 0;

        DataGenerator(CuboidClipboard schem, int level) {
            this.level = level;
            this.schem = schem;
        }

        @Override
        public void run() {
            if (level>schem.getHeight()) {
                Bukkit.getServer().getWorld(worldName).setStorm(false);
                Bukkit.getServer().getWorld(worldName).setThundering(false);

                k.loaded = true;
                return;
            }
            if (level==0) {
                k.broadcast("Setting biomes...");
                for (Block b : UtilBlock.allBlocksInside(UtilBlock.blockAt(UtilLocation.defaultWorld(), 0, 0, 0),
                        UtilBlock.blockAt(UtilLocation.defaultWorld(), schem.getLength(), 0, schem.getWidth()))) {
                    if (b==null || b.getType() != STAINED_CLAY)
                        continue;
                    Biome biome;
                    switch (b.getData()) {
                        case 3: {
                            biome = ICE_MOUNTAINS;
                            break;
                        }
                        case 4: {
                            biome = DESERT;
                            break;
                        }
                        case 5: {
                            biome = PLAINS;
                            break;
                        }
                        case 7: {
                            biome = SWAMPLAND;
                            break;
                        }
                        case 9: {
                            biome = EXTREME_HILLS_PLUS_MOUNTAINS;
                            break;
                        }
                        case 11: {
                            biome = RIVER;
                            break;
                        }
                        case 13: {
                            biome = FOREST;
                            break;
                        }
                        default: {
                            biome = PLAINS;
                            break;
                        }
                    }
                    b.setBiome(biome);
                }
            }
            k.broadcast("Generating data points: " + level + "/" + schem.getHeight());
            generateDataPoints(level);
            level++;
            new DataGenerator(schem, level).runTaskLater(k, 1);
        }
    }

    public class MapGenerator extends BukkitRunnable {

        private CuboidClipboard schem;
        private int level = 0;
        private EditSession es;

        MapGenerator(CuboidClipboard schem, int level, EditSession es) {
            this.schem = schem;
            this.level = level;
            this.es = es;
        }

        @Override
        public void run() {
            if (level>schem.getHeight()) {
                new DataGenerator(schem, 0).runTaskLater(k, 20);
                return;
            }
            k.broadcast("Generating map: " + level + "/" + schem.getHeight());
            try {
                for (int x = 0; x < schem.getLength(); x++) {
                    for (int z = 0; z < schem.getWidth(); z++) {
                        com.sk89q.worldedit.Vector v = new com.sk89q.worldedit.Vector(x, level, z);
                        BaseBlock baseBlock = schem.getBlock(v);
                        if (baseBlock == null || baseBlock.isAir())
                            continue;
                        es.setBlock(v, baseBlock);
                    }
                }
            } catch (Throwable t) {
                for (StackTraceElement st : t.getStackTrace()) {
                    Kitpvp.get().broadcast(st.toString());
                }
                Kitpvp.get().broadcast(t.getMessage());
            }
            level++;
            new MapGenerator(schem, level, es).runTaskLater(Kitpvp.get(), 1);
        }
    }

}
