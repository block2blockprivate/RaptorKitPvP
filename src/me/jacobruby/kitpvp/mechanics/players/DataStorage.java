package me.jacobruby.kitpvp.mechanics.players;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataStorage {

    private static Kitpvp k = Kitpvp.get();

    public DataStorage() {

    }

    void generateDefaultPlayerData(String uuid) {
        if (!k.getData().isSet("players." + uuid + ".balance"))
            c(uuid, "balance", k.getConfig().getInt("settings.defaultPlayerBalance"));
        if (!k.getData().isSet("players." + uuid + ".totalBalance"))
            c(uuid, "totalBalance", PlayerData.balance(uuid));
        if (!k.getData().isSet("players." + uuid + ".kits.unlocked"))
            c(uuid, "kits.unlocked", Collections.singletonList("Warrior"));
        for (PlayerKitType kit : PlayerKitType.values()) {
            if (!k.getData().isSet("players." + uuid + ".kits.stats." + kit.getName() + ".kills"))
                c(uuid, "kits.stats." + kit.getName() + ".kills", 0);
            if (!k.getData().isSet("players." + uuid + ".kits.stats." + kit.getName() + ".deaths"))
                c(uuid, "kits.stats." + kit.getName() + ".deaths", 0);
            if (!k.getData().isSet("players." + uuid + ".kits.stats." + kit.getName() + ".highestKillStreak"))
                c(uuid, "kits.stats." + kit.getName() + ".highestKillStreak", 0);
            if (!k.getData().isSet("players." + uuid + ".kits.stats." + kit.getName() + ".damageDealt"))
                c(uuid, "kits.stats." + kit.getName() + ".damageDealt", 0d);
            if (!k.getData().isSet("players." + uuid + ".kits.stats." + kit.getName() + ".damageTaken"))
                c(uuid, "kits.stats." + kit.getName() + ".damageTaken", 0d);
        }
        if (!k.getData().isSet("players." + uuid + ".stats.kills"))
            c(uuid, "stats.kills", 0);
        if (!k.getData().isSet("players." + uuid + ".stats.deaths"))
            c(uuid, "stats.deaths", 0);
        if (!k.getData().isSet("players." + uuid + ".stats.damageDealt"))
            c(uuid, "stats.damageDealt", 0d);
        if (!k.getData().isSet("players." + uuid + ".stats.damageTaken"))
            c(uuid, "stats.damageTaken", 0d);
    }

    private void c(String path, Object value) {
        k.addData(path, value);
    }

    private void c(String uuid, String path, Object value) {
        c("players."+uuid+"."+path, value);
    }

    public boolean addKit(Player p, PlayerKitType kit) {
        return addKit(p.getUniqueId().toString(), kit);
    }

    public boolean addKit(String uuid, PlayerKitType kit) {
        if (PlayerData.Kits.hasKit(uuid, kit)) {
            return false;
        } else {
            List<String> list = new ArrayList<>();
            for (PlayerKitType kits : PlayerData.Kits.getKits(uuid)) {
                list.add(kits.getName());
            }
            list.add(kit.getName());
            c(uuid, "kits.unlocked", list);
            return true;
        }
    }

    public int addBalanace(String uuid, int amount) {
        int balance = PlayerData.balance(uuid);
        balance += amount;
        c(uuid, "balance", balance);
        c(uuid, "totalBalance", PlayerData.totalBalance(uuid)+amount);
        return balance;
    }

    public int removeBalanace(String uuid, int amount) {
        int balance = PlayerData.balance(uuid);
        balance -= amount;
        c(uuid, "balance", balance);
        return balance;
    }

    public int setHighestKillStreak(String uuid, PlayerKitType kit, int amount) {
        c(uuid, "kits.stats."+kit.getName()+".highestKillStreak", amount);
        return amount;
    }

    public int setBalance(String uuid, int amount) {
        c(uuid, "balance", amount);
        return amount;
    }

    public int addGlobalKill(String uuid) {
        int kills = PlayerData.Stats.getTotalKills(uuid);
        kills++;
        c(uuid, "stats.kills", kills);
        return kills;
    }

    public int addGlobalDeath(String uuid) {
        int deaths = PlayerData.Stats.getTotalDeaths(uuid);
        deaths++;
        c(uuid, "stats.deaths", deaths);
        return deaths;
    }

    public double addGlobalDamageDealt(String uuid, double amount) {
        double damageDealt = PlayerData.Stats.getTotalDamageDealt(uuid);
        damageDealt+=amount;
        c(uuid, "stats.damageDealt", damageDealt);
        return damageDealt;
    }

    public double addGlobalDamageTaken(String uuid, double amount) {
        double damageTaken = PlayerData.Stats.getTotalDamageTaken(uuid);
        damageTaken+=amount;
        c(uuid, "stats.damageTaken", damageTaken);
        return damageTaken;
    }

    public int addGlobalSupplyDrop(String uuid) {
        int supplydrops = PlayerData.Stats.getTotalSupplyDrops(uuid);
        supplydrops++;
        c(uuid, "stats.supplyDrops", supplydrops);
        return supplydrops;
    }

    public int addKitKill(String uuid, PlayerKitType kit) {
        int kills = PlayerData.Kits.Stats.getKills(uuid, kit);
        kills++;
        c(uuid, "kits.stats."+kit.getName()+".kills", kills);
        addGlobalKill(uuid);
        return kills;
    }
    
    public int addKitDeath(String uuid, PlayerKitType kit) {
        int deaths = PlayerData.Kits.Stats.getDeaths(uuid, kit);
        deaths++;
        c(uuid, "kits.stats."+kit.getName()+".deaths", deaths);
        addGlobalDeath(uuid);
        return deaths;
    }

    public double addKitDamageDealt(String uuid, PlayerKitType kit, double amount) {
        double damageDealt = PlayerData.Kits.Stats.getDamageDealt(uuid, kit);
        damageDealt+=amount;
        c(uuid, "kits.stats."+kit.getName()+".damageDealt", damageDealt);
        addGlobalDamageDealt(uuid, amount);
        return damageDealt;
    }

    public double addKitDamageTaken(String uuid, PlayerKitType kit, double amount) {
        double damageTaken = PlayerData.Kits.Stats.getDamageTaken(uuid, kit);
        damageTaken+=amount;
        c(uuid, "kits.stats."+kit.getName()+".damageTaken", damageTaken);
        addGlobalDamageTaken(uuid, amount);
        return damageTaken;
    }

    public int addKitSupplyDrop(String uuid, PlayerKitType kit) {
        int supplydrops = PlayerData.Kits.Stats.getSupplyDrops(uuid, kit);
        supplydrops++;
        c(uuid, "kits.stats." + kit.getName() + ".supplyDrops");
        addGlobalSupplyDrop(uuid);
        return supplydrops;
    }
}
