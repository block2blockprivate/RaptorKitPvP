package me.jacobruby.kitpvp.mechanics.players;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.KitSelectionGUI;
import me.jacobruby.kitpvp.mechanics.events.timers.PlayerEffectTimer;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKit;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.mechanics.kits.abilities.Bowman;
import me.jacobruby.kitpvp.util.UtilInventory;
import me.jacobruby.kitpvp.util.UtilLocation;
import me.jacobruby.kitpvp.util.UtilVanish;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PlayerManager {

    private static Kitpvp k = Kitpvp.get();

    private Map<Player, PlayerKit> playerKitMap = new HashMap<>();
    private Map<Player, Integer> killStreakMap = new HashMap<>();
    public Set<Player> specs = new HashSet<>();
    public Set<Player> spawnProtected = new HashSet<>();

    public PlayerManager() {

    }

    public void registerPlayer(Player p) {
        k.bm.register(p);
        k.ds.generateDefaultPlayerData(p.getUniqueId().toString());
        resetPlayer(p);
    }

    public void unregisterPlayer(Player p) {
        Bowman.electricArrowPrepared.remove(p);
        CombatTag.removeTag(p);
        //PlayerEffectTimer.map.get(p).remove();
        resetPlayer(p);
    }

    public void resetPlayer(Player p) {
        UtilInventory.clear(p);
        p.setFlying(false);
        p.setAllowFlight(false);
        p.setHealth(p.getMaxHealth());
        p.setFoodLevel(20);
        p.setSaturation(20);
        p.getActivePotionEffects().forEach(po -> p.removePotionEffect(po.getType()));
        specs.remove(p);
        playerKitMap.remove(p);
        killStreakMap.put(p, 0);
        UtilVanish.unvanish(p);
        p.teleport(k.mm.spawn);
        spawnProtected.add(p);
    }

    public boolean setPlayerKit(Player p, PlayerKitType kit) {
        if (playerKitMap.get(p) == null) {
            UtilInventory.convertKit(p, kit);
            playerKitMap.put(p, new PlayerKit(kit));
            return true;
        }
        if (playerKitMap.get(p).type == kit) return false;
        UtilInventory.convertKit(p, kit);
        playerKitMap.put(p, new PlayerKit(kit));
        return true;
    }

    public void killPlayer(Player p) {
        try {
            if (getPlayerKit(p) != null) {
                int preKillStreak = PlayerData.Kits.Stats.getHighestKillStreak(p.getUniqueId().toString(), k.pm.getPlayerKit(p).type);
                int currentKillStreak = k.pm.getPlayerKillStreak(p);
                if (currentKillStreak > preKillStreak) {
                    if (currentKillStreak > 10) {
                        k.broadcast(Kitpvp.cr("&d&l" + p.getName() + "&7&l just got a personal best kill streak of &d&l" + currentKillStreak +
                                "&7&l kills using the &d&l" + k.pm.getPlayerKit(p).type.getName() + "&7&l kit!"));
                        k.ds.setHighestKillStreak(p.getUniqueId().toString(), k.pm.getPlayerKit(p).type, currentKillStreak);
                    }
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        CombatTag.removeTag(p);
        UtilInventory.clear(p);
        p.getWorld().strikeLightningEffect(p.getLocation());
        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 50, 0));
        p.setHealth(p.getMaxHealth());
        p.setFoodLevel(20);
        p.setAllowFlight(true);
        p.setFlying(true);
        UtilVanish.setInvisible(p, true);
        if (p.getLocation().getBlockY()<0)
            p.teleport(k.mm.spawn);
    }

    public void openKitSelectionGUI(Player p) {
        p.openInventory(KitSelectionGUI.gui(p));
    }

    public int getPlayerKillStreak(Player p) {
        return killStreakMap.get(p);
    }

    public int addPlayerKillStreak(Player p) {
        int i = killStreakMap.get(p);
        i++;
        killStreakMap.put(p, i);
        return i;
    }

    public PlayerKit getPlayerKit(Player p) {
        return playerKitMap.getOrDefault(p, null);
    }

    @Deprecated
    public int getPlayerKitLevel(Player p) {
        return playerKitMap.getOrDefault(p, null).level;
    }

    @Deprecated
    public int levelUpPlayerKit(Player p) {
        playerKitMap.getOrDefault(p, null).level++;
        return playerKitMap.getOrDefault(p, null).level;
    }

}
