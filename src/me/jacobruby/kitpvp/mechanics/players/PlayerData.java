package me.jacobruby.kitpvp.mechanics.players;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.configuration.file.FileConfiguration;
import java.util.ArrayList;
import java.util.List;

public class PlayerData {

    private static Kitpvp k = Kitpvp.get();
    private static FileConfiguration data = k.getData();

    private static List<String> sl(String uuid, String path) {
        return data.getStringList("players." + uuid + "." + path);
    }

    private static String s(String uuid, String path) {
        return data.getString("players." + uuid + "." + path);
    }

    private static int i(String uuid, String path) {
        return data.getInt("players." + uuid + "." + path);
    }

    private static double d(String uuid, String path) {
        return data.getDouble("players." + uuid + "." + path);
    }

    public static class Kits {

        public static List<PlayerKitType> getKits(String uuid) {
            List<PlayerKitType> kits = new ArrayList<>();
            for (String a : sl(uuid, "kits.unlocked")) {
                PlayerKitType kit;
                try {
                    kit = PlayerKitType.valueOf(a.toUpperCase());
                } catch (NullPointerException e) {
                    k.l.w(uuid + " has invalid kit " + a);
                    continue;
                }
                kits.add(kit);
            }
            return kits;
        }

        public static boolean hasKit(String uuid, PlayerKitType kit) {
            return getKits(uuid).contains(kit);
        }

        public static class Stats {
            public static int getKills(String uuid, PlayerKitType kit) {
                return i(uuid, "kits.stats." + kit.getName() + ".kills");
            }
            public static int getDeaths(String uuid, PlayerKitType kit) {
                return i(uuid, "kits.stats." + kit.getName() + ".deaths");
            }
            public static double getDamageDealt(String uuid, PlayerKitType kit) {
                return d(uuid, "kits.stats." + kit.getName() + ".damageDealt");
            }
            public static double getDamageTaken(String uuid, PlayerKitType kit) {
                return d(uuid, "kits.stats." + kit.getName() + ".damageTaken");
            }
            public static int getHighestKillStreak(String uuid, PlayerKitType kit) {
                return i(uuid, "kits.stats." + kit.getName() + ".highestKillStreak");
            }
            public static int getSupplyDrops(String uuid, PlayerKitType kit) {
                return i(uuid, "kits.stats." + kit.getName() + ".supplyDrops");
            }
        }

    }

    public static class Stats {
        public static int getTotalKills(String uuid) {
            return i(uuid, "stats.kills");
        }
        public static int getTotalDeaths(String uuid) {
            return i(uuid, "stats.deaths");
        }
        public static double getTotalDamageDealt(String uuid) {
            return d(uuid, "stats.damageDealt");
        }
        public static double getTotalDamageTaken(String uuid) {
            return d(uuid, "stats.damageTaken");
        }
        public static int getTotalSupplyDrops(String uuid) {
            return i(uuid, "stats.supplyDrops");
        }
    }

    private static class Achievements {

    }

    public static int balance(String uuid) {
        return i(uuid, "balance");
    }

    public static int totalBalance(String uuid) {
        return i(uuid, "totalBalance");
    }


}
