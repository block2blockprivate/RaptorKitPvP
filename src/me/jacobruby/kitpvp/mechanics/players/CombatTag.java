package me.jacobruby.kitpvp.mechanics.players;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;

public class CombatTag {

    private static Kitpvp k = Kitpvp.get();

    public static Map<Player, Integer> timeMap = new HashMap<>();
    
    public static void tag(Player p) {
        if (!isTagged(p)) {
            p.sendMessage(Kitpvp.c("Combat Tagger", "&cYou have been tagged! Do not log out!"));
        }
        timeMap.put(p, k.getConfig().getInt("settings.tagTime"));
    }

    public static void removeTag(Player p) {
        timeMap.remove(p);
        p.sendMessage(Kitpvp.c("Combat Tagger", "You are no longer tagged, you may log out."));
    }

    public static void update() {
        try {
            for (Player p : timeMap.keySet()) {
                int i = timeMap.get(p);
                if (i > 0) timeMap.put(p, i - 1);
                else {
                    removeTag(p);
                }
            }
        } catch (ConcurrentModificationException e) {
        }
    }

    public static boolean isTagged(Player p) {
        return timeMap.get(p)!=null;
    }

    public static int getTime(Player p) {
        return timeMap.get(p);
    }
}
