package me.jacobruby.kitpvp.mechanics.kits;

public class PlayerKit {

    public final PlayerKitType type;
    public int level = 0;

    public PlayerKit(PlayerKitType type) {
        this.type = type;
    }

    @Deprecated
    public int levelUp() {
        level++;
        return level;
    }

}
