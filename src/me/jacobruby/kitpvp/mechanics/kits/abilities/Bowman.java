package me.jacobruby.kitpvp.mechanics.kits.abilities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.util.UtilLocation;
import me.jacobruby.kitpvp.util.UtilPlayer;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.HALF;
import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.TICK;

@SuppressWarnings("unused")
public class Bowman implements Listener {
    
    private static Kitpvp k = Kitpvp.get();
    
    private static Map<Player, Integer> electricArrowMap = new HashMap<>();
    public static Set<Player> electricArrowPrepared = new HashSet<>();
    private static Set<Arrow> electricArrows = new HashSet<>();
    private static Set<Player> bowCharging = new HashSet<>();
    
    @EventHandler
    public void prepare(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction().toString().contains("LEFT")) {
            if (k.pm.getPlayerKit(p).type != PlayerKitType.BOWMAN)
                return;
            if (electricArrowMap.getOrDefault(p, null) != null)
                return;
            ItemStack hand = p.getInventory().getItemInHand();
            if (hand == null)
                return;
            if (hand.getItemMeta() == null)
                return;
            if (hand.getItemMeta().getDisplayName() == null)
                return;
            if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&bStorm")))
                return;
            e.setCancelled(true);
            if (!electricArrowPrepared.add(p)) {
                p.sendMessage(Kitpvp.c("Ability", "You already have Electric Arrow prepared!"));
                return;
            }
            p.sendMessage(Kitpvp.c("Ability", "You prepared Electric Arrow."));
            electricArrowMap.put(p, 900);
        }
    }

    @EventHandler
    public void charge(PlayerInteractEvent e) {
        if (e.getAction().toString().contains("RIGHT")) {
            Player p = e.getPlayer();
            if (k.pm.getPlayerKit(p) == null)
                return;
            if (k.pm.getPlayerKit(p).type != PlayerKitType.BOWMAN)
                return;
            if (!electricArrowPrepared.contains(p))
                return;
            ItemStack hand = p.getItemInHand();
            if (hand == null)
                return;
            if (!hand.getType().equals(Material.BOW))
                return;
            if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&bStorm")))
                return;
            bowCharging.add(p);
        }
    }

    @EventHandler
    public void bowCancel(PlayerItemHeldEvent e) {
        bowCharging.remove(e.getPlayer());
    }

    @EventHandler
    public void fire(EntityShootBowEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (k.pm.getPlayerKit(p).type != PlayerKitType.BOWMAN)
                return;
            ItemStack bow = e.getBow();
            if (!bow.getItemMeta().getDisplayName().equals(Kitpvp.cr("&bStorm")))
                return;
            if (!electricArrowPrepared.contains(p))
                return;
            p.sendMessage(Kitpvp.c("Ability", "You used Electric Arrow."));
            p.getWorld().spigot().playEffect(p.getLocation(), Effect.LAVA_POP, 0, 0, 1.5f, 1.5f, 1.5f, 1f, 20, 100);
            electricArrowPrepared.remove(p);
            bowCharging.remove(p);
            Arrow a = (Arrow) e.getProjectile();
            a.spigot().setDamage(0);
            a.setVelocity(a.getVelocity().normalize().multiply(20d));
            electricArrows.add(a);

            //Visual
            for (Vector v : UtilLocation.line(p.getEyeLocation().toVector(),
                    p.getEyeLocation().toVector().add(p.getLocation().getDirection().normalize()
                            .multiply(p.getLineOfSight((Set<Material>)null, 100).size()-1)))) {
                p.getWorld().spigot().playEffect(v.toLocation(p.getWorld()), Effect.FIREWORKS_SPARK, 0, 0, 0.1f, 0.1f, 0.1f, 2f, 3, 100);
                p.getWorld().spigot().playEffect(v.toLocation(p.getWorld()), Effect.FLAME, 0, 0, 0f, 0f, 0f, 1f,
                        1, 40);
            }
        }
    }

    @EventHandler
    public void arrowHitGround(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Arrow) {
            Arrow a = (Arrow) e.getEntity();
            if (!electricArrows.contains(a))
                return;
            electricArrows.remove(a);
            new BukkitRunnable() {
                @Override
                public void run() {
                    arrowEffect(a);
                }
            }.runTaskLater(k, 0);
        }
    }

    @EventHandler
    public void arrowHit(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow) {
            Arrow a = (Arrow) e.getDamager();
            if (e.getEntity() instanceof LivingEntity) {
                LivingEntity b = (LivingEntity) e.getEntity();
                if (!electricArrows.contains(a))
                    return;
                electricArrows.remove(a);
                e.setCancelled(true);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        arrowEffect(a);
                    }
                }.runTaskLater(k, 0);
            }
        }
    }

    private void arrowEffect(Arrow a) {
        Location loc = a.getLocation();

        //Lightning
        a.getWorld().strikeLightning(loc);
        for (Player p : UtilPlayer.all()) {

            //Explosion Knockback
            Vector c = p.getLocation().toVector().subtract(a.getLocation().toVector()).setY(0);
            double x = c.getX();
            double y = c.getY();
            double z = c.getZ();
            if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z) || c.length() == 0 || Math.abs(c.length()) > 8)
                continue;
            c.setY(c.getY() + 2);
            c.normalize();
            c.multiply(1.9);
            if (c.getY() > 5) c.setY(5);
            p.setVelocity(c);

            //Visual
            loc.getWorld().spigot().playEffect(loc, Effect.LARGE_SMOKE, 0, 0, 2.5f, 2.5f, 2.5f, 0.1f, 100, 100);

            //Potion Effects
            p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 3), true);
            p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 0), true);
            p.sendMessage(Kitpvp.c("Ability", "You've been hit with &a" + ((Player) a.getShooter()).getName() + "&7's electrified arrow."));

        }
    }

    @EventHandler
    public void effects(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Arrow a : electricArrows) {
                a.getWorld().spigot().playEffect(a.getLocation(), Effect.FIREWORKS_SPARK, 0, 0, 0.1f, 0.1f, 0.1f, 2f, 3, 100);
            }
            for (Player p : electricArrowPrepared) {
                p.getWorld().spigot().playEffect(p.getLocation(), Effect.SPELL, 0, 0, 1.2f, 1.2f, 1.2f, 2f, 2, 100);
            }
        }
        if (e.getType() == HALF) {
            for (Player p : bowCharging) {
                for (Vector v : UtilLocation.line(p.getEyeLocation().toVector(),
                        p.getEyeLocation().toVector().add(p.getLocation().getDirection().normalize()
                                .multiply(p.getLineOfSight((Set<Material>)null, 100).size()-1)), 5.0D)) {
                    p.getWorld().spigot().playEffect(v.toLocation(UtilLocation.defaultWorld()), Effect.CRIT, 0, 0, 0.05f, 0.05f, 0.05f, 1f, 2, 100);
                }
            }
        }
    }

    @EventHandler
    public void cooldown(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Player p : electricArrowMap.keySet()) {
                int i = electricArrowMap.get(p);
                i--;
                if (i<1) {
                    electricArrowMap.remove(p);
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 0.4f, 1.6f);
                    p.sendMessage(Kitpvp.c("Ability", "Your Electric Arrow ability has recharged."));
                    return;
                }
                electricArrowMap.put(p, i);
            }
        }
    }
}
