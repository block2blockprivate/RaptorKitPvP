package me.jacobruby.kitpvp.mechanics.kits.abilities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.TICK;

public class Scout implements Listener {

    private static Kitpvp k = Kitpvp.get();

    private static Map<Player, Integer> homingArrowMap = new HashMap<>();
    private static Map<Player, Integer> invisibilityMap = new HashMap<>();

    @EventHandler
    public void prepare(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction().toString().contains("LEFT")) {
            if (k.pm.getPlayerKit(p).type != PlayerKitType.SCOUT)
                return;
            if (homingArrowMap.getOrDefault(p, null) != null)
                return;
            ItemStack hand = p.getInventory().getItemInHand();
            if (hand == null)
                return;
            if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&bThe Star of Slowness")))
                return;
            e.setCancelled(true);
            p.sendMessage(Kitpvp.c("Ability", "You activated slowness."));
            homingArrowMap.put(p, 450);
            invisibilityMap.put(p, 200);
        }
    }


    @EventHandler
    public void cooldown(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Player p : homingArrowMap.keySet()) {
                int i = homingArrowMap.get(p);
                i--;
                if (i < 1) {
                    homingArrowMap.remove(p);
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 0.4f, 1.6f);
                    p.sendMessage(Kitpvp.c("Ability", "Your Slowness ability has recharged."));
                    return;
                }
                homingArrowMap.put(p, i);
            }
            for (Player p : invisibilityMap.keySet()) {
                int i2 = invisibilityMap.get(p);
                i2--;
                if (i2 < 1) {
                    invisibilityMap.remove(p);
                    p.sendMessage(Kitpvp.c("Ability", "Your Slowness ability has worn off!"));
                    return;
                }
                invisibilityMap.put(p, i2);
            }
        }
    }

    @EventHandler
    public void onPlayerDamaged(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            if (e.getDamager() instanceof Player) {
                if (k.pm.getPlayerKit((Player) e.getDamager()).type == PlayerKitType.SCOUT) {
                    if (invisibilityMap.containsKey((Player)e.getDamager())) {
                        ((Player) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 1));
                        e.getEntity().sendMessage(Kitpvp.c("Ability", e.getDamager().getName() + " has hit you with their Slowness ability!"));
                        e.getDamager().sendMessage(Kitpvp.c("Ability", "Your Slowness ability has worn off!"));
                        invisibilityMap.remove((Player) e.getDamager());
                    }
                }
            }
        }
    }
}
