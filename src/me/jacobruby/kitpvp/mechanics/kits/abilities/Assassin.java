package me.jacobruby.kitpvp.mechanics.kits.abilities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.util.UtilLocation;
import me.jacobruby.kitpvp.util.UtilPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.TICK;

public class Assassin implements Listener {

    private static Kitpvp k = Kitpvp.get();

    private static Map<Player, Integer> homingArrowMap = new HashMap<>();
    private static Map<Player, Integer> invisibilityMap = new HashMap<>();

    @EventHandler
    public void prepare(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction().toString().contains("LEFT")) {
            if (k.pm.getPlayerKit(p).type != PlayerKitType.ASSASSIN)
                return;
            if (homingArrowMap.getOrDefault(p, null) != null)
                return;
            ItemStack hand = p.getInventory().getItemInHand();
            if (hand == null)
                return;
            if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&bThe Feather of Invisibility")))
                return;
            e.setCancelled(true);
            p.sendMessage(Kitpvp.c("Ability", "You activated invisibility."));
            for (Player p2 : Bukkit.getOnlinePlayers()) {
                if (p2 == p) continue;
                p2.hidePlayer(p);
            }
            homingArrowMap.put(p, 900);
            invisibilityMap.put(p, 100);
        }
    }


    @EventHandler
    public void cooldown(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Player p : homingArrowMap.keySet()) {
                int i = homingArrowMap.get(p);
                i--;
                if (i < 1) {
                    homingArrowMap.remove(p);
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 0.4f, 1.6f);
                    p.sendMessage(Kitpvp.c("Ability", "Your Invisibility ability has recharged."));
                    return;
                }
                homingArrowMap.put(p, i);
            }
            for (Player p : invisibilityMap.keySet()){
                int i2 = invisibilityMap.get(p);
                i2--;
                if (i2 < 1) {
                    invisibilityMap.remove(p);
                    p.sendMessage(Kitpvp.c("Ability", "Your Invisibility has worn off!"));
                    for (Player p2 : Bukkit.getOnlinePlayers()) {
                        if (p2 == p) continue;
                        p2.showPlayer(p);
                    }
                    return;
                }
                invisibilityMap.put(p, i2);
            }
        } else if (e.getType() == TimerEvent.TimerType.HALF) {
            for (Player p : invisibilityMap.keySet()) {
                p.getWorld().spigot().playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 0, 0, 0.1f, 0.1f, 0.1f, 2f, 3, 100);
            }

        }
    }

    @EventHandler
    public void onPlayerDamaged(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            if (e.getDamager() instanceof Player) {
                if (k.pm.getPlayerKit((Player) e.getDamager()).type == PlayerKitType.ASSASSIN) {
                    if (invisibilityMap.containsKey((Player)e.getDamager())) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            if (p == (Player) e.getDamager()) continue;
                            p.showPlayer((Player)e.getDamager());
                        }
                        e.getDamager().sendMessage(Kitpvp.c("Ability", "Your invisibility has worn off!"));
                        invisibilityMap.remove((Player) e.getDamager());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        for (Player p : invisibilityMap.keySet()) {
            e.getPlayer().hidePlayer(p);
        }
    }
}
