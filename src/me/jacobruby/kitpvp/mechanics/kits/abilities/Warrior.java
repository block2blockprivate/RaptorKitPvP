package me.jacobruby.kitpvp.mechanics.kits.abilities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.*;
import static me.jacobruby.kitpvp.mechanics.kits.PlayerKitType.*;

@SuppressWarnings("unused")
public class Warrior implements Listener {

    private static Kitpvp k = Kitpvp.get();

    private Map<Player, Integer> slowMap = new HashMap<>();
    private Map<Player, Integer> chargeMap = new HashMap<>();

    @EventHandler
    public void slow(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            if (!(e.getEntity() instanceof LivingEntity))
                return;
            LivingEntity t = (LivingEntity) e.getEntity();
            Player p = (Player) e.getDamager();
            if (k.pm.getPlayerKit(p).type != WARRIOR)
                return;
            if (slowMap.getOrDefault(p, null) != null)
                return;
            ItemStack hand = p.getItemInHand();
            if (hand == null||hand.getItemMeta().getDisplayName() == null)
                return;
            if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&9Cleaver")))
                return;
            p.getWorld().playSound(t.getLocation(), Sound.IRONGOLEM_HIT, 1f, 0.8f);
            t.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 80, 1, true, false), true);
            t.sendMessage(Kitpvp.c("Ability", "&a" + p.getName() + "&7 hit you with their mighty Cleaver."));
            p.sendMessage(Kitpvp.c("Ability", "Your mighty Cleaver slows your target."));
            slowMap.put(p, 150);
        }
    }

    @EventHandler
    public void charge(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (!e.getAction().toString().contains("RIGHT"))
            return;
        if (k.pm.getPlayerKit(p) == null)
            return;
        if (k.pm.getPlayerKit(p).type != WARRIOR)
            return;
        if (chargeMap.getOrDefault(p, null) != null)
            return;
        ItemStack hand = p.getItemInHand();
        if (hand == null)
            return;
        if (!hand.getItemMeta().getDisplayName().equals(Kitpvp.cr("&9Cleaver"))||hand.getItemMeta().getDisplayName() == null)
            return;
        e.setCancelled(true);
        Vector dir = p.getLocation().getDirection();
        dir.normalize();
        dir.multiply(2.1);
        dir.setY(0.7);
        p.setVelocity(dir);
        p.setFallDistance(0f);
        p.getWorld().playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 2f, 1.4f);
        p.sendMessage(Kitpvp.c("Ability", "You harvest the power of the mighty Cleaver to charge forward."));
        chargeMap.put(p, 250);
    }

    @EventHandler
    public void cooldown(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Player p : slowMap.keySet()) {
                int i = slowMap.get(p);
                i--;
                if (i<1) {
                    slowMap.remove(p);
                    return;
                }
                slowMap.put(p, i);
            }
            for (Player p : chargeMap.keySet()) {
                int i = chargeMap.get(p);
                i--;
                if (i<1) {
                    chargeMap.remove(p);
                    p.playSound(p.getLocation(), Sound.ORB_PICKUP, 0.4f, 1.6f);
                    p.sendMessage(Kitpvp.c("Recharge", "Your Charge ability has recharged."));
                    return;
                }
                chargeMap.put(p, i);
            }
        }
    }

}
