package me.jacobruby.kitpvp.mechanics.kits;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.Enchant;
import me.jacobruby.kitpvp.mechanics.entities.Kit;
import me.jacobruby.kitpvp.util.KitCreator;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import static org.bukkit.Material.*;

public class KitLoader {

    private static Kitpvp k = Kitpvp.get();

    private static KitLoader instance;

    public KitLoader() {
        instance = this;
    }

    public static KitLoader get() {
        return instance;
    }

    Kit Warrior() {
        KitCreator kit = new KitCreator("Warrior", 1000);
        kit.description(
                "&7This classic kit is given," +
                "&7to all who seek adventure," +
                "&7and power!,"
        );
        kit.helmet(IRON_HELMET, "&7" + kit.name()+"'s Helmet").chestplate(IRON_CHESTPLATE, "&7" + kit.name()+"'s Chestplate")
                .leggings(IRON_LEGGINGS, "&7" + kit.name()+"'s Leggings").boots(DIAMOND_BOOTS, "&7" + kit.name()+"'s Boots", new Enchant(Enchantment.PROTECTION_FALL, 2));
        kit.hb(1, IRON_SWORD, 1, "&7" + kit.name()+"'s Broadsword");
        kit.potion(true, 2, "&c"+kit.name()+"'s Shield Potion", null, 1, new PotionEffect(PotionEffectType.ABSORPTION, 500, 2));
        kit.hb(9, COOKED_BEEF, 12, "&7" + kit.name()+"'s Beef");
        kit.hb(3, FISHING_ROD, 1, "&7" + kit.name()+"'s Fishing Rod", new Enchant(Enchantment.DURABILITY, 200));
        kit.hb(4, COMPASS, 1, "&b&lTracking Compass");
        return kit.create();
    }

    Kit Bowman() {
        KitCreator kit = new KitCreator("Bowman", 1000);
        kit.description(
                "&7This sharp eyed kit is," +
                "&7the master of ranged warfare."
        );
        kit.helmet(LEATHER_HELMET, "&7" + kit.name() + "'s Cap", (String) null, new Enchant(Enchantment.PROTECTION_PROJECTILE, 1));
        kit.chestplate(CHAINMAIL_CHESTPLATE, "&7" + kit.name() + "'s Breastplate");
        kit.leggings(CHAINMAIL_LEGGINGS, "&7" + kit.name() + "'s Pants");
        kit.boots(IRON_BOOTS, "&7" + kit.name() + "'s Shoes", (String) null, new Enchant(Enchantment.PROTECTION_FALL, 7));
        kit.hb(1, BOW, 1, "Bow of the Man", "&8Many have said that one,&8must be a true man in order,&8to use this ancient bow.,&8It also whacks hardly.", new Enchant(Enchantment.ARROW_DAMAGE, 2), new Enchant(Enchantment.KNOCKBACK, 1), new Enchant(Enchantment.ARROW_INFINITE, 1));
        kit.hb(2, ENDER_PEARL, 3, "&9" + kit.name() + "'s Pearl", "&5Teleport thingy.", new Enchant(Enchantment.SILK_TOUCH, 1));
        kit.hb(3, STONE_SWORD, 1, "&7" + kit.name() + "'s Knife");
        kit.hb(9, COOKED_CHICKEN, 6, "&7" + kit.name() + "'s Poultry");
        kit.i(19, ARROW, 1, "&e" + kit.name() + "'s Arrow");
        kit.hb(4, FISHING_ROD, 1, "&7" + kit.name()+"'s Fishing Rod", new Enchant(Enchantment.DURABILITY, 200));
        kit.hb(5, COMPASS, 1, "&b&lTracking Compass");
        return kit.create();
    }

    Kit Assassin() {
        KitCreator kit = new KitCreator("Assassin", 1500);
        kit.description(
                "&7This kit has harnessed," +
                        "&7the ability of invisibility," +
                        "&7and being super sneaky!"
        );
        kit.helmet(CHAINMAIL_HELMET, "&7" + kit.name() + "'s Helmet");
        kit.chestplate(LEATHER_CHESTPLATE, "&7" + kit.name() + "'s Chestplate");
        kit.leggings(LEATHER_LEGGINGS, "&7" + kit.name() + "'s Leggings");
        kit.boots(CHAINMAIL_BOOTS, "&7" + kit.name() + "'s Boots");
        kit.hb(1, STONE_SWORD, 1, "&7" + kit.name() + "'s Blade");
        kit.hb(2, BOW, 1, "&7" + kit.name() + "'s Bow", (String) null, new Enchant(Enchantment.ARROW_INFINITE, 1), new Enchant(Enchantment.ARROW_DAMAGE, 3));
        kit.hb(9, COOKED_CHICKEN, 9, "&7" + kit.name() + "'s Poultry");
        kit.i(19, ARROW, 1, "&e" + kit.name() + "'s Arrow");
        kit.hb(3, FISHING_ROD, 1, "&7" + kit.name()+"'s Fishing Rod", new Enchant(Enchantment.DURABILITY, 200));
        kit.hb(4, COMPASS, 1, "&b&lTracking Compass");
        return kit.create();
    }

    Kit Scout() {
        KitCreator kit = new KitCreator("Scout", 1250);
        kit.description(
                "&7This kit has harnessed," +
                        "&7the ability speed and is able," +
                        "&7to manipulate his and other speed!"
        );
        kit.helmet(CHAINMAIL_HELMET, "&7" + kit.name() + "'s Helmet");
        kit.chestplate(IRON_CHESTPLATE, "&7" + kit.name() + "'s Chestplate");
        kit.leggings(CHAINMAIL_LEGGINGS, "&7" + kit.name() + "'s Leggings");
        kit.boots(IRON_BOOTS, "&7" + kit.name() + "'s Boots");
        kit.hb(1, STONE_SWORD, 1, "&7" + kit.name() + "'s Blade", (String) null, new Enchant(Enchantment.DAMAGE_ALL, 1));
        kit.potion(true , 2, "&b" + kit.name() + "Speed Potion", null, 1, new PotionEffect(PotionEffectType.SPEED, 12000, 0));
        kit.hb(9, COOKED_CHICKEN, 9, "&7" + kit.name() + "'s Poultry");
        kit.hb(3, FISHING_ROD, 1, "&7" + kit.name()+"'s Fishing Rod", new Enchant(Enchantment.DURABILITY, 200));
        kit.hb(4, COMPASS, 1, "&b&lTracking Compass");
        return kit.create();
    }


}