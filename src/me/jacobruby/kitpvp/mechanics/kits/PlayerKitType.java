package me.jacobruby.kitpvp.mechanics.kits;

import me.jacobruby.kitpvp.mechanics.entities.Kit;
import me.jacobruby.kitpvp.util.UtilItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

import static org.bukkit.Material.*;

public enum PlayerKitType {
    WARRIOR("Warrior", KitLoader.get().Warrior(), 20, IRON_CHESTPLATE, UtilItem.Bank.gapple("Warrior"), /*UtilItem.ci(
            ENCHANTED_BOOK, "&e&lWarrior's Book", 1, "&7This ancient book is essentially,&7an upgrade to your current kit.,&7Right " +
                    "click to apply!"),*/
            UtilItem.Bank.cleaver(), new ItemStack(Material.COOKED_CHICKEN, 6)
    ),
    BOWMAN("Bowman", KitLoader.get().Bowman(), 21, BOW, UtilItem.Bank.gapple("Bowman"), /*UtilItem.ci(ENCHANTED_BOOK,
            "&e&lBowman's Book", 1, "&7This ancient book is essentially,&7an upgrade to your current kit.,&7Right " +
                    "click to apply!"),*/
            UtilItem.Bank.storm(), UtilItem.Bank.gapple("Bowman"), UtilItem.Bank.gapple("Bowman"), new ItemStack(Material.COOKED_BEEF, 12)
    ),
    ASSASSIN("Assassin", KitLoader.get().Assassin(), 22, LEATHER_CHESTPLATE, UtilItem.Bank.gapple("Assassin"), /*UtilItem.ci(ENCHANTED_BOOK,
            "&e&lBowman's Book", 1, "&7This ancient book is essentially,&7an upgrade to your current kit.,&7Right " +
                    "click to apply!"),*/
            UtilItem.Bank.sniper(), UtilItem.Bank.gapple("Assassin"), UtilItem.Bank.gapple("Assassin")
    ),
    SCOUT("Scout", KitLoader.get().Scout(), 23, NETHER_STAR, UtilItem.Bank.gapple("Scout"), /*UtilItem.ci(ENCHANTED_BOOK,
            "&e&lBowman's Book", 1, "&7This ancient book is essentially,&7an upgrade to your current kit.,&7Right " +
                    "click to apply!"),*/
            UtilItem.Bank.slowness(), UtilItem.Bank.gapple("Scout"), UtilItem.Bank.gapple("Scout")
    );

    private String name;
    private Kit kit;
    private int GUISlot;
    private Material icon;
    private List<ItemStack> supplyDropLoot;

    PlayerKitType(String name, Kit kit, int GUISlot, Material icon, ItemStack... items) {
        this.name = name;
        this.kit = kit;
        this.GUISlot = GUISlot;
        this.icon = icon;
        this.supplyDropLoot = Arrays.asList(items);
    }

    public String getName() { return name; }

    public Kit getKit() { return kit; }

    public int getGUISlot() { return GUISlot; }

    public Material getIcon() { return icon; }

    public List<ItemStack> getLoot() { return supplyDropLoot; }
}
