package me.jacobruby.kitpvp.mechanics.scoreboard;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKit;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import me.jacobruby.kitpvp.util.UtilScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class BoardManager implements Listener {

    private static Kitpvp k = Kitpvp.get();

    //Map<Player, Scoreboard> map = new HashMap<>();

    public void register(Player p) {
        Scoreboard board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
        Objective obj = board.registerNewObjective("display", "dummy");
        Team kit = board.registerNewTeam("kit");
        kit.addEntry(ChatColor.RED.toString());
        Team killstreak = board.registerNewTeam("killstreak");
        killstreak.addEntry(ChatColor.BLUE.toString());
        Team balance = board.registerNewTeam("balance");
        balance.addEntry(ChatColor.GREEN.toString());
        p.setScoreboard(board);

        obj.setDisplayName(Kitpvp.cr("&9&m------&b &lKITPVP&9 &m------"));
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.getScore(ChatColor.DARK_AQUA.toString()).setScore(14);  //
        obj.getScore(Kitpvp.cr("&3» &b&lKit")).setScore(13);         //» Kit
        obj.getScore(ChatColor.RED.toString()).setScore(12);        //<Kit>
        obj.getScore(ChatColor.DARK_BLUE.toString()).setScore(11);  //
        obj.getScore(Kitpvp.cr("&3» &b&lKill Streak")).setScore(10); //» Kill Streak
        obj.getScore(ChatColor.BLUE.toString()).setScore(9);        //<Kill streak>
        obj.getScore(ChatColor.DARK_GRAY.toString()).setScore(8);   //
        obj.getScore(Kitpvp.cr("&3» &b&lBalance")).setScore(7);      //» Balance
        obj.getScore(ChatColor.GREEN.toString()).setScore(6);       //<Balance>
        obj.getScore(ChatColor.DARK_GREEN.toString()).setScore(5);  //
        obj.getScore(Kitpvp.cr("&9&owww.raptorpvp.net")).setScore(4);
        obj.getScore(Kitpvp.c("&9&m----------------")).setScore(3); //----------------
    }

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType() == TimerEvent.TimerType.SECOND) {
            Bukkit.getServer().getOnlinePlayers().forEach(this::updatePlayer);
        }
    }

    private void updatePlayer(Player p) {
        Scoreboard board = p.getScoreboard();
        try {
            Team balance = board.getTeam("balance");
            balance.setPrefix(String.valueOf(PlayerData.balance(p.getUniqueId().toString())));
            PlayerKit playerKit = k.pm.getPlayerKit(p);
            Team kit = board.getTeam("kit");
            kit.setPrefix((playerKit != null ? (playerKit.type.getName()) : "None"));
            Team killstreak = board.getTeam("killstreak");
            killstreak.setPrefix(String.valueOf(k.pm.getPlayerKillStreak(p)));
        } catch (Throwable e) {
            for (StackTraceElement st : e.getStackTrace()) {
                k.l.s("Line " + st.getLineNumber() + "\n" + st.toString());
            }
            k.l.s(e.getMessage());
        }
    }

}
