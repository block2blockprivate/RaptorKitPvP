package me.jacobruby.kitpvp.mechanics.events.timers;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.players.CombatTag;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

@SuppressWarnings("unused")
public class CombatTagTimer implements Listener {

    private static Kitpvp k = Kitpvp.get();
    private static Block block;

    public Player getNearestPlayer(Player player) {
        Location location = player.getLocation();
        double distNear = 2147486470.0;
        Player playerNear = null;
        for (Player player2 : Bukkit.getOnlinePlayers()) {
            //don't include the player that's checking
            if (player == player2) { continue; }

            Location location2 = player.getLocation();
            double dist = location.distance(location2);
            if (playerNear == null || dist < distNear) {
                playerNear = player2;
                distNear = dist;
            }
        }
        return playerNear;
    }

    public static void setBlock(Block b) { block = b; }

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType() == TimerEvent.TimerType.HALF) {
            for (Player a : Bukkit.getOnlinePlayers()) {
                if (SupplyDrop.isPlayerAlive) {
                    try {
                        a.setCompassTarget(SupplyDrop.supplyDropPlayer.getLocation());
                    } catch (NullPointerException e2) {
                        Bukkit.getLogger().info("Unable to set compass target, supplyDropPlayer = null.");
                    }
                } else {
                    try {
                        a.setCompassTarget(block.getLocation());
                    } catch (NullPointerException e2) {
                       Bukkit.getLogger().info("Unable to set compass target, block = null.");
                    }

                }

            }
        }
        if (e.getType() == TimerEvent.TimerType.SECOND) {
            CombatTag.update();
        }

    }
}
