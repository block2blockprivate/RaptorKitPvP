package me.jacobruby.kitpvp.mechanics.events.timers;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ConcurrentModificationException;
import java.util.Random;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.*;

@SuppressWarnings("unused")
public class SupplyDropTimer implements Listener {

    private static Kitpvp k = Kitpvp.get();
    
    public static int delay = 300;

    public static int chooseRan(int min, int max){
        Random rn = new Random();
        return rn.nextInt(max - min + 1) + min;
    }

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType() == TICK)
            try {
                SupplyDrop.map.keySet().forEach(i -> SupplyDrop.map.get(i).update());
            } catch (ConcurrentModificationException b) {
            }
        if (e.getType() == SECOND) {
            if (!SupplyDrop.isAlive) {
                if (SupplyDrop.getState() == SupplyDrop.State.CLOSED||SupplyDrop.getState() == null) {
                    if (Bukkit.getOnlinePlayers().size() == 0) {
                        return;
                    }
                    Block block = k.mm.supplyDropList.get(chooseRan(0, k.mm.supplyDropList.size() - 1));
                    if (SupplyDrop.map.containsKey(block)) {
                        for (Block a : k.mm.supplyDropList) {
                            block = a;
                            if (!SupplyDrop.map.containsKey(block))
                                break;
                        }
                    }
                    CombatTagTimer.setBlock(block);
                    new SupplyDrop(block);
                }
            }
        }
    }

}
