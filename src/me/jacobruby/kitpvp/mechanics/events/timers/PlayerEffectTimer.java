package me.jacobruby.kitpvp.mechanics.events.timers;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.events.listeners.PlayerMoveListener;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

import static me.jacobruby.kitpvp.mechanics.events.TimerEvent.TimerType.TICK;

@SuppressWarnings("unused")
public class PlayerEffectTimer implements Listener {

    private static Kitpvp k = Kitpvp.get();

    //public static Map<Player, ArmorStand> map = new HashMap<>();

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType() == TICK) {
            for (Block b : k.mm.pads) {
                Location loc = b.getRelative(BlockFace.UP).getLocation().add(0.5, 0, 0.5);
                loc.getWorld().spigot().playEffect(loc, Effect.HAPPY_VILLAGER,0, 0, 0.3f, 0.3f, 0.3f, 0.3f, 1, 30);
            }
            /*
            try {
                for (ArmorStand a : map.values()) {
                    if (a.isDead())
                        for (Player p : map.keySet()) {
                            if (map.get(p) == a) {
                                map.remove(p);
                                return;
                            }
                        }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                p.setMaxHealth(40);

                //Hologram
                try {
                    ArmorStand s;
                    if (!map.containsKey(p)) {
                        s = (ArmorStand) p.getWorld().spawnEntity(p.getLocation(), EntityType.ARMOR_STAND);
                        s.setCustomNameVisible(true);
                        s.setVisible(false);
                        s.setGravity(false);
                        s.setMarker(true);
                        s.setBasePlate(false);
                        s.setArms(true);
                        s.setRightArmPose(new EulerAngle(275d, 0d, 0d));
                        map.put(p, s);
                    } else s = map.get(p);
                    PlayerKitType kit;
                    if (k.pm.getPlayerKit(p) == null)
                        kit = null;
                    else
                        kit = k.pm.getPlayerKit(p).type;


                    //s.setItemInHand(new ItemStack(kit == null ? Material.BARRIER : kit.getIcon()));
                    s.setCustomName(Kitpvp.cr("&3» &b&lKit: &f" + (kit == null ? "None" : kit.getName())));
                    Location loc1 = p.getLocation().clone();
                    loc1.setYaw(p.getLocation().getYaw() - 35);
                    Vector a = loc1.getDirection().setY(0).normalize();
                    a.multiply(4);
                    Location fin = p.getLocation().add(a).add(0d, 2d, 0);
                    fin.setYaw(loc1.getYaw()-180);
                    s.teleport(fin);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
            */
        }
    }


}
