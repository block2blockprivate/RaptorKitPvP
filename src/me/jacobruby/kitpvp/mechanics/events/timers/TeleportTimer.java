package me.jacobruby.kitpvp.mechanics.events.timers;

import me.jacobruby.kitpvp.mechanics.commands.SpawnCommand;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

@SuppressWarnings("unused")
public class TeleportTimer implements Listener {

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType() == TimerEvent.TimerType.SECOND) {
            SpawnCommand.update();
        }
    }


}
