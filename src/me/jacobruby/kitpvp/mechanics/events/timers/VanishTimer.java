package me.jacobruby.kitpvp.mechanics.events.timers;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.util.UtilVanish;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

@SuppressWarnings("unused")
public class VanishTimer implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void timer(TimerEvent e) {
        if (e.getType().equals(TimerEvent.TimerType.HALF)) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (k.pm.specs.contains(p)) {
                    UtilVanish.vanish(p);
                }
            }
        }
    }


}
