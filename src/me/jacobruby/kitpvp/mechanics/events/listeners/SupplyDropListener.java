package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

@SuppressWarnings("unused")
public class SupplyDropListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void rightClickChest(PlayerInteractEvent e) {
        if (e.getAction()==Action.RIGHT_CLICK_BLOCK) {
            Player a = e.getPlayer();
            if (k.pm.getPlayerKit(a) == null)
                return;
            Block b = e.getClickedBlock();
            if (SupplyDrop.map.containsKey(b)) {
                e.setCancelled(true);
                SupplyDrop sd = SupplyDrop.map.get(b);
                if (sd.state==SupplyDrop.State.DROPPED) {
                    sd.open(a);
                }
            }
        }
    }
}
