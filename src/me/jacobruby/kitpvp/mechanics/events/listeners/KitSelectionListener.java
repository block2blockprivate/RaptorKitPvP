package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.KitSelectionGUI;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import me.jacobruby.kitpvp.util.UtilBlock;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class KitSelectionListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    public static Map<Villager, Location> npcs = new HashMap<>();

    @EventHandler
    public void rightClickVillager(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Villager) {
            Villager npc = (Villager) e.getRightClicked();
            if (ChatColor.stripColor(npc.getName()).equals("Kit Selection")) {
                e.setCancelled(true);
                Player p = e.getPlayer();
                if (k.pm.specs.contains(p))
                    return;
                if (k.pm.getPlayerKit(p) == null) {
                    k.pm.openKitSelectionGUI(p);
                } else {
                    p.sendMessage(Kitpvp.c("Kits", "&cYou have already selected a kit! You must " +
                            "be killed or relog to reset."));
                }
            }
        }
    }

    @EventHandler
    public void villager(TimerEvent e) {
        if (e.getType() == TimerEvent.TimerType.TICK) {
            for (Villager npc : npcs.keySet()) {
                npc.setProfession(Villager.Profession.PRIEST);
                npc.teleport(npcs.get(npc));
                npc.setCustomName(Kitpvp.cr("&a&lKit Selection"));
                npc.setCustomNameVisible(true);
                npc.setMaxHealth(9001);
                npc.setHealth(2048);
                net.minecraft.server.v1_8_R3.Entity ent = ((CraftEntity)npc).getHandle();
                NBTTagCompound nbt = ent.getNBTTag();
                if (nbt == null)
                    nbt = new NBTTagCompound();
                ent.c(nbt);
                nbt.setInt("NoAI", 1);
                nbt.setInt("Invulnerable", 1);
                ent.f(nbt);
            }
        }
    }

    @EventHandler
    public void GUIClick(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            Player p = (Player) e.getWhoClicked();
            List<PlayerKitType> kits = PlayerData.Kits.getKits(p.getUniqueId().toString());
            if (e.getClickedInventory().getName().equals(KitSelectionGUI.gui(p).getName())) {
                e.setCancelled(true);
                int slot = e.getSlot();
                for (PlayerKitType kit : PlayerKitType.values()) {
                    if (slot == kit.getGUISlot()) {
                        if (kits.contains(kit)) {
                            k.pm.setPlayerKit(p, kit);
                            p.sendMessage(Kitpvp.c("Kits", "You equipped the &a" + kit.getName() + "&7 kit."));
                            p.closeInventory();
                        } else {
                            try {
                                p.openInventory(KitSelectionGUI.confirm(kit));
                            } catch (Throwable t) {
                                for (StackTraceElement st : t.getStackTrace()) {
                                    k.l.s("Line " + st.getLineNumber() + "\n" + st.toString());
                                }
                                k.l.s(t.getMessage());
                            }
                        }
                    }
                }
            } else {
                for (PlayerKitType kit : PlayerKitType.values()) {
                    if (e.getClickedInventory().getName().equals(KitSelectionGUI.confirm(kit).getName())) {
                        e.setCancelled(true);
                        ItemStack clicked = e.getCurrentItem();
                        if (clicked.getItemMeta().getDisplayName().equals(Kitpvp.c("&a&lYes"))) {
                            if (PlayerData.balance(p.getUniqueId().toString())>=kit.getKit().price()) {
                                k.pm.setPlayerKit(p, kit);
                                k.ds.addKit(p, kit);
                                k.ds.removeBalanace(p.getUniqueId().toString(), kit.getKit().price());
                                p.sendMessage(Kitpvp.c("Kits", "You purchased the &a" + kit.getName() + "&7 kit."));
                                k.pm.openKitSelectionGUI(p);
                            } else {
                                p.sendMessage(Kitpvp.c("Kits", "&cYou do not have enough money to purchase this kit!"));
                                p.closeInventory();
                            }
                        }
                        if (clicked.getItemMeta().getDisplayName().equals(Kitpvp.c("&c&lCancel"))) {
                            k.pm.openKitSelectionGUI(p);
                        }
                    }
                }
            }
        }
    }
}
