package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.commands.SpawnCommand;
import me.jacobruby.kitpvp.util.UtilLocation;
import me.jacobruby.kitpvp.util.UtilPlayer;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

@SuppressWarnings("unused")
public class PlayerMoveListener implements Listener {

    private static Kitpvp k = Kitpvp.get();



    @EventHandler
    public void playerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        Location to = e.getTo();
        Location from = e.getFrom();

        //Spawn command movement cancellation
        if (SpawnCommand.isInMap(p)) {
            if (e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
                SpawnCommand.stop(p);
                p.playSound(p.getLocation(), Sound.NOTE_BASS, 0.7f, 0.2f);
            }
        }

        //Boost Pads
        if (UtilPlayer.isOnGround(p)) {
            if (k.mm.pads.contains(p.getLocation().getBlock().getRelative(BlockFace.DOWN))) {
                p.setVelocity(p.getLocation().getDirection().multiply(3.5).setY(2));
                p.getWorld().playSound(p.getLocation(), Sound.IRONGOLEM_HIT, 2f, 0.4f);
            }
        }

        //Spawn Protection
        if (!UtilLocation.isInSpawn(p.getLocation())) {
            if (k.pm.getPlayerKit(p) == null) {
                p.sendMessage(Kitpvp.c("KitPvP", "You must select a kit before you can play!"));
                k.pm.resetPlayer(p);
                if (!k.pm.spawnProtected.contains(p)) {
                    k.pm.spawnProtected.add(p);
                }
            }
        }

        //Border
        if (p.getGameMode() != GameMode.CREATIVE) {
            if (to.getY()!=from.getY()) {
                int x = p.getLocation().getBlockX();
                int z = p.getLocation().getBlockZ();
                if (!(x > 75 && x < 200 && z > 75 && z < 200)) {
                    for (Block b : k.mm.borders) {
                        if (x == b.getX() && z == b.getZ()) {
                            p.sendMessage(Kitpvp.c("Border", "Woah there, restriced grounds m8, turn back please, kthxbai."));
                            Vector v = k.mm.spawn.toVector().subtract(p.getLocation().toVector()).setY(0.1);
                            v.normalize().multiply(0.8);
                            p.setVelocity(v);
                            break;
                        }
                    }
                }
            }
        }
    }

}
