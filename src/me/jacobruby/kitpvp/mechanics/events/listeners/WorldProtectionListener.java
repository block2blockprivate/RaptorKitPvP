package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.util.UtilBlock;
import me.jacobruby.kitpvp.util.UtilLocation;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

@SuppressWarnings("unused")
public class WorldProtectionListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void timer(TimerEvent t) {

    }

    @EventHandler
    public void blockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        Location loc = p.getLocation().clone();
        if (p.getGameMode() != GameMode.CREATIVE) {
            e.setCancelled(true);
            if (UtilBlock.isSolid(e.getBlock()))
                p.teleport(loc);
        }
    }

    @EventHandler
    public void blockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        Location loc = p.getLocation().clone();
        if (p.getGameMode() != GameMode.CREATIVE) {
            e.setCancelled(true);
            p.teleport(loc);
        }
    }

    @EventHandler
    public void inventoryProtection(InventoryClickEvent e) {
        String a = e.getCurrentItem().getType().name();
        //Armor
        if (e.getWhoClicked().getGameMode() == GameMode.CREATIVE)
            return;
        if (a.matches("[A-Z]{4,9}_CHESTPLATE") || a.matches("[A-Z]{4,9}_HELMET") || a.matches("[A-Z]{4,9}_LEGGINGS") ||
                a.matches("[A-Z]{4,9}_BOOTS")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void dropItem(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if (p.getGameMode() == GameMode.CREATIVE)
            return;
        e.setCancelled(true);
    }

    @EventHandler
    public void customEnderPearl(PlayerTeleportEvent e) {
        if (e.getCause() != PlayerTeleportEvent.TeleportCause.ENDER_PEARL)
            return;
        e.setCancelled(true);
        e.getPlayer().setNoDamageTicks(5);
        e.getPlayer().teleport(e.getTo());
    }

    @EventHandler
    public void villager(LightningStrikeEvent e) {
        for (Entity entity : e.getLightning().getWorld().getNearbyEntities(e.getLightning().getLocation(), 3, 3, 3)) {
            if (!(entity instanceof Villager))
                return;
            Villager npc = (Villager) entity;
            if (!KitSelectionListener.npcs.containsKey(npc))
                return;
            Location loc = npc.getLocation().clone();
            npc.teleport(new Location(UtilLocation.defaultWorld(), 0d, 0d, 0d));
            new BukkitRunnable() {
                @Override
                public void run() {
                    npc.teleport(loc);
                }
            }.runTaskLater(k, 1);
        }
    }
}
