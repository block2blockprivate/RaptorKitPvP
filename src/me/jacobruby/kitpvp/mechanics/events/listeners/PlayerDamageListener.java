package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.players.CombatTag;
import me.jacobruby.kitpvp.util.UtilLocation;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

@SuppressWarnings("unused")
public class PlayerDamageListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void playerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (UtilLocation.isInSpawn(p.getLocation()) || k.pm.specs.contains(p)) {
                e.setCancelled(true);
            } else if (k.pm.spawnProtected.contains(p) && e.getCause() == EntityDamageEvent.DamageCause.FALL) {
                k.pm.spawnProtected.remove(p);
                e.setDamage(0);
                Kitpvp.startMessage(p);
                e.setCancelled(true);
            } else if (UtilLocation.isInSpawn(p.getLocation())) {
                e.setDamage(0);
                e.setCancelled(true);
            } else {
                if (k.pm.spawnProtected.contains(p)) {
                    Kitpvp.startMessage(p);
                    k.pm.spawnProtected.remove(p);
                }
                //Log player damage taken
                if (k.pm.getPlayerKit(p) != null) k.ds.addKitDamageTaken(p.getUniqueId().toString(), k.pm.getPlayerKit(p).type, e.getFinalDamage());
                if (e instanceof EntityDamageByEntityEvent) {
                    EntityDamageByEntityEvent e2 = (EntityDamageByEntityEvent) e;
                    Entity entity = e2.getDamager();
                    final Player d;
                    //Test if arrow shot by player
                    if (entity instanceof Projectile) {
                        if (!(((Projectile) entity).getShooter() instanceof Player)) {
                            return;
                        }
                        d = (Player) ((Projectile) entity).getShooter();
                    } else if (entity instanceof Player) {
                        d = (Player) e2.getDamager();
                    } else return;
                    //Check if they are a spectator
                    if (k.pm.specs.contains(d) || k.pm.specs.contains(p)) {
                        e.setCancelled(true);
                    } else {
                        CombatTag.tag(p);
                        //Log player damage dealt
                        if (k.pm.getPlayerKit(d) != null)
                            k.ds.addKitDamageDealt(d.getUniqueId().toString(), k.pm.getPlayerKit(d).type, e.getFinalDamage());
                        //Send action bar messages
                        IChatBaseComponent message1 = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + Kitpvp.c("&e" + d.getName() +
                                " hit you for " + Math.floor(e.getFinalDamage() * 10) / 10 + " damage (" +
                                e.getOriginalDamage(EntityDamageEvent.DamageModifier.BASE)) + ")\"}");
                        PacketPlayOutChat packet1 = new PacketPlayOutChat(message1, (byte) 2);
                        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet1);

                        IChatBaseComponent message2 = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + Kitpvp.c("&eYou hit " + p.getName() +
                                " for " + Math.floor(e.getFinalDamage() * 10) / 10 + " damage (" +
                                e.getOriginalDamage(EntityDamageEvent.DamageModifier.BASE)) + ")\"}");
                        PacketPlayOutChat packet2 = new PacketPlayOutChat(message2, (byte) 2);
                        ((CraftPlayer) d).getHandle().playerConnection.sendPacket(packet2);
                    }
                }
            }
        }
    }


}
