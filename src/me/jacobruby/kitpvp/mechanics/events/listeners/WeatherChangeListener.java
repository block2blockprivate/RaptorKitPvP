package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChangeListener implements Listener {

    private Kitpvp k = Kitpvp.get();

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e){
        if (k.loaded) {
            if (e.toWeatherState()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onThunderChange(ThunderChangeEvent e){
        if (k.loaded) {
            if (e.toThunderState()) {
                e.setCancelled(true);
            }
        }
    }

}
