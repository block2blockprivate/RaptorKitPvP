package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;

@SuppressWarnings("unused")
public class PlayerJoinListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler(priority = EventPriority.HIGH)
    public void playerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.setMaxHealth(40);
        k.pm.registerPlayer(p);
        //Fail safing the player storage;
        try {
            OfflinePlayer op = (OfflinePlayer) p;
            k.addData("uuids." + p.getName().toLowerCase(), op);
        } catch (Exception ex) {
            k.l.s(ex.getMessage());
        }
    }

    @EventHandler
    public void playerAccess(AsyncPlayerPreLoginEvent e) {
        if (Bukkit.getOfflinePlayer(e.getUniqueId()).isOp())
            return;
        if (!k.loaded)
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, Kitpvp.c("&7Sorry! The KitPvP map is currently still generating! Please be patient, this can take up to 10 minutes."));
    }
}
