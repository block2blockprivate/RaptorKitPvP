package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class HungerListener implements Listener {

    public static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void onHungerChange(FoodLevelChangeEvent e) {
        if (k.pm.spawnProtected.contains(Bukkit.getPlayer(e.getEntity().getUniqueId()))) {
            e.setFoodLevel(30);
        }
    }
}
