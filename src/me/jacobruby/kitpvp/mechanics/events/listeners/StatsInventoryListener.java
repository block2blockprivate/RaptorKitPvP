package me.jacobruby.kitpvp.mechanics.events.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;

@SuppressWarnings("unused")
public class StatsInventoryListener implements Listener {

    @EventHandler
    public void click(InventoryClickEvent e) {
        Inventory inv = e.getClickedInventory();
        if (inv.getName() == null)
            return;
        String invName = ChatColor.stripColor(inv.getName());
        if (invName.matches("[a-zA-Z0-9_]{1,16}'s Stats")){
            e.setCancelled(true);
            if (e.getSlot() == 0)
                e.getWhoClicked().closeInventory();
        }

    }

    @EventHandler
    public void protect(InventoryMoveItemEvent e) {
        Inventory inv1 = e.getDestination();
        Inventory inv2 = e.getInitiator();
        String inv1Name;
        String inv2Name;
        try {
            inv1Name = ChatColor.stripColor(inv1.getName());
            inv2Name = ChatColor.stripColor(inv2.getName());
        } catch (NullPointerException n) {
            return;
        }
        if (inv1Name.matches("[a-zA-Z0-9_]{1,16}'s Stats") || inv2Name.matches("[a-zA-Z0-9_]{1,16}'s Stats")){
            e.setCancelled(true);
        }
    }


}
