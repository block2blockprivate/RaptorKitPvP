package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKit;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

@Deprecated
@SuppressWarnings("unused")
public class KitLevelerListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void rightClick(PlayerInteractEvent e) {
        if (!e.getAction().toString().contains("RIGHT"))
            return;
        Player a = e.getPlayer();
        ItemStack b = a.getItemInHand();
        if (b == null)
            return;
        PlayerKit kit = k.pm.getPlayerKit(a);
        if (kit==null)
            return;
        if (!b.getItemMeta().getDisplayName().equals(Kitpvp.c("&e&l"+kit.type.getName()+"'s Book")))
            return;
        e.setCancelled(true);
        a.setItemInHand(null);

    }
}
