package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import me.jacobruby.kitpvp.mechanics.players.CombatTag;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@SuppressWarnings("unused")
public class PlayerQuitListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler
    public void playerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (CombatTag.isTagged(p)) {
            p.setHealth(1);
            try {
                Entity ent = ((EntityDamageByEntityEvent) p.getLastDamageCause()).getDamager();
                p.damage(5, (ent instanceof Player) ? ent : ((ent instanceof Projectile) ? ((((Projectile) ent).getShooter()
                        instanceof Entity) ? ((Entity) ((Projectile) ent).getShooter()) : null) : ent));
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        if (SupplyDrop.supplyDropPlayer != null){
            if (SupplyDrop.supplyDropPlayer.equals(e.getPlayer())) {
                SupplyDrop.supplyDropPlayer = null;
                SupplyDrop.isAlive = false;
                SupplyDrop.isPlayerAlive = false;
            }
        }
        k.pm.unregisterPlayer(p);
        //Fail safing the player storage;
        try {
            OfflinePlayer op = (OfflinePlayer) p;
            k.addData("uuids." + p.getName().toLowerCase(), op);
        } catch (Exception ex) {
            k.l.s(ex.getMessage());
        }
    }
}
