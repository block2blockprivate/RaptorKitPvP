package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import me.jacobruby.kitpvp.util.UtilItem;
import me.jacobruby.kitpvp.util.UtilLocation;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;

@SuppressWarnings("unused")
public class PlayerDeathListener implements Listener {

    private static Kitpvp k = Kitpvp.get();

    @EventHandler(ignoreCancelled = true)
    public void playerDamageEvent(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            double prehealth = p.getHealth();
            double posthealth = prehealth - e.getFinalDamage();
            if (k.pm.spawnProtected.contains(p)) {
                if (!UtilLocation.isInSpawn(p.getLocation())) {
                    k.pm.spawnProtected.remove(p);
                    Kitpvp.startMessage(p);
                }
                e.setDamage(0);
                e.setCancelled(true);
                return;
            }
            if (posthealth <= 0) {
                p.setHealth(p.getMaxHealth());
                e.setDamage(0);
                try {
                    k.pm.killPlayer(p);
                    if (k.pm.getPlayerKit(p) != null) {
                        k.ds.addKitDeath(p.getUniqueId().toString(), k.pm.getPlayerKit(p).type);
                        if (e instanceof EntityDamageByEntityEvent) {
                            EntityDamageByEntityEvent e2 = (EntityDamageByEntityEvent) e;
                            if (e2.getDamager() instanceof Player || e2.getDamager() instanceof Projectile) {
                                final Player killer;
                                if (e2.getDamager() instanceof Projectile) {
                                    if (!(((Projectile) e2.getDamager()).getShooter() instanceof Player)) {
                                        return;
                                    }
                                    killer = (Player) ((Projectile)e2.getDamager()).getShooter();
                                } else {
                                    killer = (Player) e2.getDamager();
                                }
                                if (k.pm.getPlayerKit(killer) == null)
                                    return;
                                UtilItem.giveKill(killer);
                                k.broadcast(Kitpvp.c("Death", "&a" + p.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                        + "&2) &7was killed by &a" + killer.getName() + "&2 (&a" + k.pm.getPlayerKit(killer).type.getName()
                                        + "&2)&7"));
                                killer.sendMessage(Kitpvp.c("Kill", "You killed &a" + p.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                        + "&2)&7."));
                                killer.sendMessage(Kitpvp.c("Stats", "You now have &a" + k.ds.addKitKill(killer.getUniqueId().toString(),
                                        k.pm.getPlayerKit(killer).type) + "&7 kills with &a" + k.pm.getPlayerKit(killer).type.getName() + "&7."));
                                final int ks = k.pm.addPlayerKillStreak(killer);
                                killer.sendMessage(Kitpvp.c("Kill Streak",  "You are now on a &a" + ks
                                        + "&7 player kill streak."));
                                k.ds.addBalanace(killer.getUniqueId().toString(), 25);
                                killer.sendMessage(Kitpvp.c("Bank", "You gained &a$25&7 for killing &a" + p.getName() + "&7."));
                                switch (ks) {
                                    case 5:
                                        k.broadcast(Kitpvp.c("Kill Streak", "&a" + killer.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                                + "&2)&7 is now on a &a5&7 player kill streak!"));
                                        k.ds.addBalanace(killer.getUniqueId().toString(), 100);
                                        killer.sendMessage(Kitpvp.c("Bank", "You gained an extra &a$100&7 for getting a &a5&l player kill streak."));
                                        break;
                                    case 10:
                                        k.broadcast(Kitpvp.c("Kill Streak", "&a" + killer.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                                + "&2)&7 is now on a &a&l10&7 player kill streak!"));
                                        k.ds.addBalanace(killer.getUniqueId().toString(), 250);
                                        killer.sendMessage(Kitpvp.c("Bank", "You gained an extra &a$250&7 for getting a &a10&7 player kill streak."));
                                        break;
                                    case 15:
                                        k.broadcast(Kitpvp.c("Kill Streak", "&a" + killer.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                                + "&2)&7 is now on a &c&l15&7 player kill streak!"));
                                        k.ds.addBalanace(killer.getUniqueId().toString(), 500);
                                        killer.sendMessage(Kitpvp.c("Bank", "You gained an extra &a$500&7 for getting a &a15&7 player kill streak."));
                                        break;
                                    default: {
                                        if (ks > 19) {
                                            k.broadcast(Kitpvp.c("Kill Streak", "&a" + killer.getName() + "&2 (&a" + k.pm.getPlayerKit(p).type.getName()
                                                    + "&2)&7 is now on a &4&l&n" + ks + "&7 player kill streak!"));
                                            Bukkit.getServer().getOnlinePlayers().forEach(player ->
                                                    player.playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 0.8f, 1f));
                                            k.ds.addBalanace(killer.getUniqueId().toString(), 100);
                                            killer.sendMessage(Kitpvp.c("Bank", "You gained an extra &a$100&7 for getting a &a" + ks + "&7 player kill streak."));
                                        }
                                    }
                                }
                            } else {
                                p.sendMessage(Kitpvp.c("Death", "You died."));
                            }
                        } else {
                            p.sendMessage(Kitpvp.c("Death", "You died."));
                        }
                    }
                } catch (Throwable throwable) {
                    for (StackTraceElement st : throwable.getStackTrace()) {
                        k.l.s("Line " + st.getLineNumber() + "\n" + st.toString());
                    }
                    k.l.s(throwable.getMessage());
                }
                p.sendMessage(Kitpvp.c("Death", "You will respawn in 10 seconds."));
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        k.pm.resetPlayer(p);
                        p.sendMessage(Kitpvp.c("Respawn", "You were respawned."));
                    }
                }.runTaskLater(Kitpvp.get(), 200);
                if (SupplyDrop.supplyDropPlayer.equals(e.getEntity())) {
                    SupplyDrop.supplyDropPlayer = null;
                    SupplyDrop.isAlive = false;
                    SupplyDrop.isPlayerAlive = false;
                }
            }
        }
    }


}
