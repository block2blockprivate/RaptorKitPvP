package me.jacobruby.kitpvp.mechanics.events.listeners;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

    private Kitpvp k = Kitpvp.get();

    @EventHandler
    public void onServerPing(ServerListPingEvent e) {
        if (k.loaded) {
            e.setMotd(Kitpvp.cr("&aOnline"));
        } else {
            e.setMotd(Kitpvp.cr("&cOffline"));
        }
    }
}
