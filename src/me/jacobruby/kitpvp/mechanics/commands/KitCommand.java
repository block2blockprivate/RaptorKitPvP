package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KitCommand implements CommandExecutor {

    private static Kitpvp k = Kitpvp.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("rank.admin")) {
            if (args.length == 0) {
                sender.sendMessage(Kitpvp.c("Commands", "Usage: &d/kit <Player> <Kit>"));
                try {
                    k.pm.openKitSelectionGUI((Player) sender);
                } catch (Throwable t) {
                    Bukkit.getServer().broadcastMessage(t.getMessage());
                    for (StackTraceElement s : t.getStackTrace()) {
                        Bukkit.getServer().broadcastMessage("Line " + s.getLineNumber() + ":\n" + s.toString());
                    }
                }
            } else {
                Player p = Bukkit.getServer().getPlayer(args[0]);
                if (p == null) {
                    sender.sendMessage(Kitpvp.c("Commands", "Player " + args[0] + " was not found!"));
                } else {
                    if (args.length == 1) {
                        sender.sendMessage(Kitpvp.c("Kit", "&a" + p.getName() + "&7's kit is &a" + (k.pm.getPlayerKit(p)!=null?
                                k.pm.getPlayerKit(p).type.getName():("None")) + "&7."));
                    } else {
                        String a = args[1];

                        PlayerKitType kit;
                        try {
                            kit = PlayerKitType.valueOf(a.toUpperCase());
                        } catch (NullPointerException e) {
                            sender.sendMessage(Kitpvp.c("Commands", "That kit does not exist."));
                            return true;
                        }
                        if (k.pm.setPlayerKit(p, kit)) {
                            p.sendMessage(Kitpvp.c("Kit", "&a" + sender.getName() + "&7 has set your " +
                                    "kit to &a" + kit.getName() + "&7."));
                            sender.sendMessage(Kitpvp.c("Commands", "Equipped kit &a" +
                                    kit.getName() + "&7 to &a" + p.getName() + "&7."));
                        } else {
                            sender.sendMessage(Kitpvp.c("Commands", p.getName() + " already has the &a" + kit.getName() +
                                    "&7 kit equipped."));
                        }
                    }
                }
            }
        } else {
            sender.sendMessage(Kitpvp.c("&2Command Manager>>&c Sorry! That command is unknown!"));
        }
        return true;
    }
}
