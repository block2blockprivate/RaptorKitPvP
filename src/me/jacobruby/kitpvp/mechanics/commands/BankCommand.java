package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BankCommand implements CommandExecutor {

    private static Kitpvp k = Kitpvp.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("rank.admin")) {
            if (args.length == 0) {
                sender.sendMessage(Kitpvp.c("Commands", "Usage: &d/bank <Player> [set:remove:add]"));
            } else {
                String a = args[0];
                OfflinePlayer t = k.getData().getOfflinePlayer("uuids." + a.toLowerCase());
                if (t == null) {
                    sender.sendMessage(Kitpvp.c("Commands", "Player " + args[0] + " was not found!"));
                } else {
                    if (args.length == 1) sender.sendMessage(Kitpvp.c("Bank", "Player &d" + t.getName() + "&7 has a balance of: &d$" +
                            String.valueOf(PlayerData.balance(t.getUniqueId().toString()))));
                    else {
                        if (args.length == 2) {
                            sender.sendMessage(Kitpvp.c("Commands", "Usage: &d/bank <Player> <set:remove:add> <Amount>"));
                        } else {
                            String b = args[1].toLowerCase();
                            String c = args[2];
                            if (c.matches("[0-9]+")) {
                                int d = Integer.parseInt(c);
                                switch (b) {
                                    case "set": {
                                        k.ds.setBalance(t.getUniqueId().toString(), d);
                                        try {
                                            ((Player) t).sendMessage(Kitpvp.c("Bank", "Your balance was set to &d$" + c));
                                        } catch (Exception e) {
                                            k.l.w(e.getMessage());
                                        }
                                        sender.sendMessage(Kitpvp.c("Bank", "&d" + t.getName() + "&7's balance set to &d$" + c));
                                        break;
                                    }
                                    case "add": {
                                        int balance = k.ds.addBalanace(t.getUniqueId().toString(), d);
                                        try {
                                            ((Player) t).sendMessage(Kitpvp.c("Bank", "Your balance was set to &d$" + balance));
                                        } catch (Exception e) {
                                            k.l.w(e.getMessage());
                                        }
                                        sender.sendMessage(Kitpvp.c("Bank", "&d" + t.getName() + "&7's balance set to &d$" + balance));
                                        break;
                                    }
                                    case "remove": {
                                        int balance = k.ds.removeBalanace(t.getUniqueId().toString(), d);
                                        try {
                                            ((Player) t).sendMessage(Kitpvp.c("Bank", "Your balance was set to &d$" + balance));
                                        } catch (Exception e) {
                                            k.l.w(e.getMessage());
                                        }
                                        sender.sendMessage(Kitpvp.c("Bank", "&d" + t.getName() + "&7's balance set to &d$" + balance));
                                        break;
                                    }
                                }
                            } else {
                                sender.sendMessage(Kitpvp.c("Bank", "&cThat is not a valid number!"));
                            }
                        }
                    }
                }
            }
        } else {
            sender.sendMessage(Kitpvp.c("&2Command Manager>>&c Sorry! That command is unknown!"));
        }
        return true;
    }
}
