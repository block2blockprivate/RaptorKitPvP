package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.SupplyDrop;
import me.jacobruby.kitpvp.mechanics.events.timers.CombatTagTimer;
import me.jacobruby.kitpvp.mechanics.events.timers.SupplyDropTimer;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static me.jacobruby.kitpvp.mechanics.events.timers.SupplyDropTimer.chooseRan;

public class SupplyDropCommand implements CommandExecutor {

    private Kitpvp k = Kitpvp.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("rank.admin")) {
            sender.sendMessage(Kitpvp.c("Command Manager", "&cSorry! That command is unknown!"));
            return true;
        }
        if (args.length == 0) {
            sender.sendMessage(Kitpvp.c("Supply Drop", "Spawning a supply drop!"));
            if (!SupplyDrop.isAlive) {
                if (Bukkit.getOnlinePlayers().size() == 0) {
                    return true;
                }
                Block block = k.mm.supplyDropList.get(chooseRan(0, k.mm.supplyDropList.size() - 1));
                if (SupplyDrop.map.containsKey(block)) {
                    for (Block a : k.mm.supplyDropList) {
                        block = a;
                        if (!SupplyDrop.map.containsKey(block))
                            break;
                    }
                }
                CombatTagTimer.setBlock(block);
                new SupplyDrop(block);
            }
        } else if (args.length == 1) {
            sender.sendMessage(Kitpvp.c("Supply Drop", "Spawning a supply drop in " + args[0] + " seconds!"));
            if (Bukkit.getOnlinePlayers().size() == 0) {
                return true;
            }
            Block block = k.mm.supplyDropList.get(chooseRan(0, k.mm.supplyDropList.size() - 1));
            if (SupplyDrop.map.containsKey(block)) {
                for (Block a : k.mm.supplyDropList) {
                    block = a;
                    if (!SupplyDrop.map.containsKey(block))
                        break;
                }
            }
                CombatTagTimer.setBlock(block);
                new SupplyDrop(block);
        } else {
            sender.sendMessage(Kitpvp.c("Supply Drop", "That is not a valid number!"));
        }
        return true;
    }
}
