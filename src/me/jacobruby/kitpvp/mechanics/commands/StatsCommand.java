package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.StatsGUI;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatsCommand implements CommandExecutor {

    private static Kitpvp k = Kitpvp.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("rank.player"))
            return true;
        if (!(sender instanceof Player))
            return true;
        Player p = (Player) sender;
        if (args.length==0) {
            try {
                p.openInventory(StatsGUI.gui(p.getUniqueId().toString()));
            } catch (Throwable e) {
                for (StackTraceElement st : e.getStackTrace()) {
                    Bukkit.getServer().broadcastMessage("Line " + st.getLineNumber() + "\n" + st.toString());
                }
                Bukkit.getServer().broadcastMessage(e.getMessage());
            }
            return true;
        }
        OfflinePlayer t = k.getData().getOfflinePlayer("uuids." + args[0].toLowerCase());
        if (t == null) {
            sender.sendMessage(Kitpvp.c("Commands", "Player " + args[0] + " was not found!"));
            return true;
        }
        try {
            p.openInventory(StatsGUI.gui(t.getUniqueId().toString()));
        } catch (Throwable e) {
            for (StackTraceElement st : e.getStackTrace()) {
                Bukkit.getServer().broadcastMessage("Line " + st.getLineNumber() + "\n" + e.toString());
            }
            Bukkit.getServer().broadcastMessage(e.getMessage());
        }
        return true;
    }
}
