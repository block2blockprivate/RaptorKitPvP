package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.players.PlayerManager;
import me.jacobruby.kitpvp.util.UtilLocation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class SpawnCommand implements CommandExecutor {

    private static Kitpvp k = Kitpvp.get();

    private static Map<Player, Integer> timeMap = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            start(p);
        }
        return true;
    }

    public static void update() {
        for (Player p : timeMap.keySet()) {
            int i = timeMap.get(p);
            if (i>0) timeMap.put(p, i-1); else {
                end(p);
            }
        }
    }

    public static void start(Player p) {
        if (timeMap.containsKey(p)) {
            p.sendMessage(Kitpvp.c("Teleport", "You are already teleporting!"));
            return;
        }
        p.sendMessage(Kitpvp.c("Teleport", "Teleport started! Don't move for &d" + k.getConfig().getInt("settings.teleportTime")
                + "&7 seconds!"));
        timeMap.put(p, k.getConfig().getInt("settings.teleportTime"));
    }

    public static void stop(Player p) {
        timeMap.remove(p);
        p.sendMessage(Kitpvp.c("Teleport", "&cMovement detected, teleport canceled!"));
    }

    private static void end(Player p) {
        timeMap.remove(p);
        p.sendMessage(Kitpvp.c("Teleport", "You were teleported to spawn."));
        k.pm.resetPlayer(p);
    }

    public static boolean isInMap(Player p) {
        return timeMap.containsKey(p);
    }
}
