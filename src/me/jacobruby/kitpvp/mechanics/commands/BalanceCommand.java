package me.jacobruby.kitpvp.mechanics.commands;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BalanceCommand implements CommandExecutor {

    private static Kitpvp k = Kitpvp.get();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("rank.player")) {
            if (args.length == 0 || !sender.hasPermission("rank.staff")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    p.sendMessage(Kitpvp.c("Bank", "Your balance is &d$" + PlayerData.balance(p.getUniqueId().toString())));
                } else {
                    sender.sendMessage(Kitpvp.c("Bank", "You cannot check your own balance as you are not a Player!"));
                }
            } else {
                OfflinePlayer t = k.getData().getOfflinePlayer("uuids." + args[0].toLowerCase());
                if (t == null) {
                    sender.sendMessage(Kitpvp.c("Commands", "Player " + args[0] + " was not found!"));
                } else {
                    if (args.length == 1) sender.sendMessage(Kitpvp.c("Bank", "Player &d" + t.getName() + "&7 has a balance of: &d$" +
                            String.valueOf(PlayerData.balance(t.getUniqueId().toString()))));
                }
            }
        }
        return true;
    }
}
