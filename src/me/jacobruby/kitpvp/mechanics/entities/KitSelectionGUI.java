package me.jacobruby.kitpvp.mechanics.entities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;

import static org.bukkit.Material.*;

public class KitSelectionGUI extends GUI {

    public static Inventory gui(Player p) {
        Inventory inv = Bukkit.createInventory(null, 54, Kitpvp.c("&a&lKit Selection"));
        List<PlayerKitType> kits = PlayerData.Kits.getKits(p.getUniqueId().toString());
        i(inv);
        for (PlayerKitType kit : PlayerKitType.values()) {
            if (kits.contains(kit)) {
                i(inv, kit.getGUISlot(), kit.getIcon(), "&a&l" + kit.getName(), 1, kit.getKit().lore(),
                        (short)0, true);
            } else {
                i(inv, kit.getGUISlot(), kit.getIcon(), "&c&l" + kit.getName(), 1,
                        "&ePrice: &7,&a" +
                        kit.getKit().price() + "," +
                        kit.getKit().lore() + ",&a&lClick to purchase!");
            }
        }
        return inv;
    }

    public static Inventory confirm(PlayerKitType kit) {
        Inventory inv = Bukkit.createInventory(null, 36, Kitpvp.cr("&c&lPurchase " + kit.getName() + "&c&l?"));
        for (int i = inv.getSize()-1; i>-1; i--) {
            i(inv, i, STAINED_GLASS_PANE, "Are you sure?", 1, null, (short)7);
        }
        i(inv, 4, kit.getIcon(), "&e&l" + kit.getName(), 1, kit.getKit().lore(), (short)0, true);
        int[] green = new int[]{9, 10, 11, 18, 19, 20, 27, 28, 29};
        int[] red = new int[]{15, 16, 17, 24, 25, 26, 33, 34, 35};
        int[] decor = new int[]{12, 14, 30, 32};
        for (int i : green) {
            i(inv, i, STAINED_CLAY, "&a&lYes", 1, null, (short)5);
        }
        for (int i : red) {
            i(inv, i, STAINED_CLAY, "&c&lCancel", 1, null, (short)14);
        }
        for (int i : decor) {
            i(inv, i, STAINED_GLASS_PANE, "Are you sure?");
        }
        return inv;
    }

}
