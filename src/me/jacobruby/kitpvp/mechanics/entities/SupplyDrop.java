package me.jacobruby.kitpvp.mechanics.entities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.util.UtilPlayer;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.Blocks;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockAction;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftFirework;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.HashMap;
import java.util.Map;

import static me.jacobruby.kitpvp.mechanics.entities.SupplyDrop.State.*;
import static org.bukkit.Material.*;
import static org.bukkit.block.BlockFace.*;

public class SupplyDrop extends GUI {

    private static Kitpvp k = Kitpvp.get();
    public static Map<Block, SupplyDrop> map = new HashMap<>();
    public int timer = -1;
    public static State state;
    public static Block block;
    public static boolean isAlive = false;
    public static boolean isPlayerAlive = false;
    public static Player supplyDropPlayer = null;

    public SupplyDrop(Block block) {
        this.block = block;
        state = DROPPING;
        spawn();
        timer = 600;
        map.put(block, this);
        isAlive = true;

    }

    public static State getState() { return state; }

    public void update() {
        if (timer>0) timer--;
        if (timer==0) {
            switch (state) {
                case DROPPING: {
                    placeChest();
                    timer = -1;
                    return;
                }
                case OPENED: {
                    remove();
                    timer = -1;
                    return;
                }
            }
        } else if (timer>0 && timer<255 && state != OPENED) {
            effect();
        }
    }

    private void effect() {
        Location loc = block.getLocation().add(0.5, 0.5, 0.5).add(0, timer, 0);
        Firework f = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fm = f.getFireworkMeta();
        fm.setPower(1);
        fm.addEffect(FireworkEffect.builder().with(FireworkEffect.Type.STAR).withColor(Color.AQUA).build());
        f.setFireworkMeta(fm);
        ((CraftFirework)f).getHandle().expectedLifespan = 1;
        //f.detonate();
    }

    private void spawn() {
        Block c = block.getRelative(DOWN);
        c.setType(STAINED_GLASS);
        c.getRelative(DOWN).setType(BEACON);
        Block d = c.getRelative(DOWN).getRelative(DOWN);
        Block[] e = new Block[]{d, d.getRelative(NORTH), d.getRelative(NORTH_EAST), d.getRelative(EAST),
                d.getRelative(SOUTH_EAST), d.getRelative(SOUTH), d.getRelative(SOUTH_WEST), d.getRelative(WEST),
                d.getRelative(NORTH_WEST)};
        for (Block f : e) {
            f.setType(IRON_BLOCK);
        }
        k.broadcast(Kitpvp.c("&b&lA supply drop is spawning at " + block.getX() + "&b&l, " + block.getZ() + "&b&l!"));
        UtilPlayer.forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_PLING, 2f, 1f));
    }

    private void placeChest() {
        try {
            Firework fw = (Firework) block.getWorld().spawnEntity(block.getLocation().add(0.5, 0.5, 0.5), EntityType.FIREWORK);
            FireworkMeta fwm = fw.getFireworkMeta();
            fwm.setPower(1);
            fwm.addEffect(FireworkEffect.builder().flicker(true).trail(true).with(FireworkEffect.Type.STAR).withColor(Color.GREEN,
                    Color.AQUA, Color.LIME).withFade(Color.TEAL, Color.SILVER, Color.MAROON, Color.ORANGE).build());
            fw.setFireworkMeta(fwm);
            ((CraftFirework)fw).getHandle().expectedLifespan = 1;
            //fw.detonate();
            block.setType(CHEST);
            block.getRelative(DOWN).getRelative(DOWN).getRelative(DOWN).getRelative(NORTH).setType(AIR);
        } catch (Throwable e) {
            k.broadcast(e.getMessage());
        }
        state = DROPPED;
    }

    public void open(Player p) {
        for (Player a : Bukkit.getServer().getOnlinePlayers()) {
            BlockPosition bp = new BlockPosition(block.getX(), block.getY(), block.getZ());
            PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(bp, Blocks.CHEST, 1, 1);
            ((CraftPlayer)a).getHandle().playerConnection.sendPacket(packet);
        }
        PlayerKitType kit;
        p.openInventory(inv(kit = k.pm.getPlayerKit(p).type));
        k.ds.addKitSupplyDrop(p.getUniqueId().toString(), kit);
        state = OPENED;
        timer = 200;
        supplyDropPlayer = p;
        isPlayerAlive = true;
        Bukkit.getServer().broadcastMessage(Kitpvp.c("Supply Drop", p.getName() + " has collected the supply drop! Kill them to spawn another one!"));
    }

    private void remove() {
        block.setType(AIR);
        block.getWorld().getNearbyEntities(block.getLocation(), 0.1, 2, 0.1).stream().filter(e -> e instanceof ArmorStand).
                filter(e -> {
                    ArmorStand a = (ArmorStand) e;
                    return ChatColor.stripColor(a.getCustomName()).equals("Supply Drop") ||
                            ChatColor.stripColor(a.getCustomName()).equals("Looted");
                }).forEach(Entity::remove);
        state = CLOSED;
        map.remove(this.block);
    }

    private static Inventory inv(PlayerKitType kit) {
        Inventory inv = Bukkit.createInventory(null, 9, Kitpvp.cr("&b&l" + kit.getName() + "&b&l Supplies"));
        inv.addItem((ItemStack[]) kit.getLoot().toArray());
        return inv;
    }

    public static Block getBlock() {
        return block;
    }

    public enum State {
        DROPPING, DROPPED, OPENED, CLOSED
    }
}
