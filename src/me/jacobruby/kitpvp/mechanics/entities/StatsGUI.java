package me.jacobruby.kitpvp.mechanics.entities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import me.jacobruby.kitpvp.mechanics.players.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

import static me.jacobruby.kitpvp.mechanics.players.PlayerData.Stats.*;
import static me.jacobruby.kitpvp.mechanics.players.PlayerData.Kits.Stats.*;

public class StatsGUI extends GUI {

    public static Inventory gui(String uuid) {
        String name = Bukkit.getServer().getOfflinePlayer(UUID.fromString(uuid)).getName();
        Inventory inv = Bukkit.createInventory(null, 45, Kitpvp.cr("&e&l" +
                name + "&e&l's Stats"));
        i(inv, 0, Material.BOW, "&lBack");
        i(inv, 13, Material.NETHER_STAR, "&e&lStats", 1, " ," +
                "&eBalance:&7 " + PlayerData.balance(uuid) + "," +
                "&eTotal Money Earned:&7 " + PlayerData.totalBalance(uuid) + "," +
                " ," +
                "&eTotal Kills:&7 " + getTotalKills(uuid) + "," +
                "&eTotal Deaths:&7 " + getTotalDeaths(uuid) + "," +
                "&eKill/Death Ratio:&7 " + getTotalKills(uuid)/(getTotalDeaths(uuid)>0?getTotalDeaths(uuid):1) + "," +
                " ," +
                "&eTotal Damage Dealt:&7 " + round(getTotalDamageDealt(uuid)) + "," +
                "&eTotal Damage Taken:&7 " + round(getTotalDamageTaken(uuid)) + "," +
                " ");
        for (PlayerKitType kit : PlayerKitType.values()) {
            if (PlayerData.Kits.getKits(uuid).contains(kit))
                i(inv, kit.getGUISlot(), kit.getIcon(), "&e&l" + name + "'s " + kit.getName() + " stats", 1, " ," +
                        "&eKills:&7 " + getKills(uuid, kit) + "," +
                        "&eDeaths:&7 " + getDeaths(uuid, kit) + "," +
                        "&eKill/Death Ratio:&7 " + getKills(uuid, kit)/(getDeaths(uuid, kit)>0?getDeaths(uuid, kit):1) + "," +
                        " ," +
                        "&eHighest Kill Streak:&7 " + getHighestKillStreak(uuid, kit) + "," +
                        " ," +
                        "&eDamage Dealt:&7 " + round(getDamageDealt(uuid, kit)) + "," +
                        "&eDamage Taken:&7 " + round(getDamageTaken(uuid, kit)) + "," +
                        " ");
        }
        return inv;
    }
    
    private static double round(double d) {
        return Math.floor(d*10)/10;
    }
}
