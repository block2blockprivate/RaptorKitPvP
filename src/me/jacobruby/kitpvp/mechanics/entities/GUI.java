package me.jacobruby.kitpvp.mechanics.entities;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.util.UtilItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

import static org.bukkit.Material.STAINED_GLASS_PANE;

class GUI extends UtilItem {

    static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing, String skullName) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Kitpvp.c(name));
        if (lore != null) {
            im.setLore(Arrays.asList(Kitpvp.c(lore).split(",")));
        }
        if (glowing) {
            im.addEnchant(Enchantment.DURABILITY, 1, true);
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        is.setItemMeta(im);
        inv.setItem(slot, is);
    }
    static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data, boolean glowing) {
        i(inv, slot, type, name, amount, lore, data, glowing, null);
    }
    static void i(Inventory inv, int slot, Material type, String name, int amount, String lore, short data) {
        i(inv, slot, type, name, amount, lore, data, false);
    }
    static void i(Inventory inv, int slot, Material type, String name, int amount, String lore) {
        i(inv, slot, type, name, amount, lore, (short)0);
    }
    static void i(Inventory inv, int slot, Material type, String name, int amount) {
        i(inv, slot, type, name, amount, null);
    }
    static void i(Inventory inv, int slot, Material type, String name) {
        i(inv, slot, type, name, 1);
    }
    static void i(Inventory inv, int slot, Material type) {
        i(inv, slot, type, "");
    }
    static void i(Inventory inv) {
        for (int i = inv.getSize()-1; i>-1; i--) {
            if (inv.getItem(i) == null) {
                i(inv, i, STAINED_GLASS_PANE, " ");
            }
        }
        int[] a = new int[]{10,11,12,13,14,15,16,19,25,28,34,37,38,39,40,41,42,43};
        for (int i : a) {
            i(inv, i, STAINED_GLASS_PANE, " ", 1, null, (short)7);
        }
    }
}
