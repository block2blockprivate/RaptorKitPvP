package me.jacobruby.kitpvp.core;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sun.istack.internal.NotNull;
import me.jacobruby.kitpvp.mechanics.commands.*;
import me.jacobruby.kitpvp.mechanics.events.TimerEvent;
import me.jacobruby.kitpvp.mechanics.events.listeners.*;
import me.jacobruby.kitpvp.mechanics.events.timers.*;
import me.jacobruby.kitpvp.mechanics.kits.KitLoader;
import me.jacobruby.kitpvp.mechanics.kits.abilities.Assassin;
import me.jacobruby.kitpvp.mechanics.kits.abilities.Bowman;
import me.jacobruby.kitpvp.mechanics.kits.abilities.Scout;
import me.jacobruby.kitpvp.mechanics.kits.abilities.Warrior;
import me.jacobruby.kitpvp.mechanics.mapgeneration.MapManager;
import me.jacobruby.kitpvp.mechanics.players.CombatTag;
import me.jacobruby.kitpvp.mechanics.players.DataStorage;
import me.jacobruby.kitpvp.mechanics.players.PlayerManager;
import me.jacobruby.kitpvp.mechanics.scoreboard.BoardManager;
import me.jacobruby.kitpvp.util.UtilPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Kitpvp extends JavaPlugin {

    /**
     * TODO Make more kits
     *
     *
     *
     *
     *
     */



    private static Kitpvp instance;
    public KitpvpLogger l;
    public BoardManager bm;
    public PlayerManager pm;
    public DataStorage ds;
    public CombatTag ct;
    public WorldEditPlugin we;
    public MapManager mm;

    private File configFile;
    private FileConfiguration config;
    private File dataFile;
    private FileConfiguration data;

    public boolean loaded = false;


    @Override
    public void onEnable() {
        instance = this;
        try {
            l = new KitpvpLogger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        bootstrap();
    }

    @Override
    public void onDisable() {
        instance = null;
        for (Player p : getServer().getOnlinePlayers()) {
            p.kickPlayer(c("Server Management", "This server is being restarted. Please be patient while it is starting back up."));
        }
        
    }

    private void bootstrap() {
        setConfig();
        setInstances();
        registers();
        loadMap();
    }

    private void registers() {
        try {
            new KitLoader();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            registerListeners(new PlayerDeathListener(), new PlayerDamageListener(), new PlayerJoinListener(),
                    new PlayerQuitListener(), new KitSelectionListener(), new VanishTimer(), new TeleportTimer(),
                    new CombatTagTimer(), new PlayerMoveListener(), new PlayerEffectTimer(), new StatsInventoryListener(),
                    new BoardManager(), new Warrior(), new Bowman(), new Assassin(), new Scout(), new SupplyDropTimer(), new SupplyDropListener(),
                    new WorldProtectionListener(), new WeatherChangeListener(), new HungerListener(), new ServerPingListener());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            registerCommands();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            registerTimers();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void setConfig() {
        try {
            loadConfig();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            addConfigDefaults();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void setInstances() {
        try {
            we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        } catch (Exception e) {
            l.s("World edit was not found in the plugin libraries!");
        }
        try {
            mm = new MapManager(config.getString("settings.map.name"), config.getString("settings.map.schematic"));
        } catch (FileNotFoundException e) {
            l.s(e.getMessage());
        }
        try {
            bm = new BoardManager();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            pm = new PlayerManager();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            ds = new DataStorage();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            ct = new CombatTag();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void registerTimers() {
        /*
        for (TimerEvent.TimerType type : TimerEvent.TimerType.values()) {
            getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
                @Override
                public void run() {
                    getServer().getPluginManager().callEvent(new TimerEvent(type));
                }
            },0,type.getTicks());

        }
        */
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(TimerEvent.TimerType.TICK));
            }
        }, 0, 0);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(TimerEvent.TimerType.HALF));
            }
        }, 0, 10);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                getServer().getPluginManager().callEvent(new TimerEvent(TimerEvent.TimerType.SECOND));
            }
        }, 0, 20);
    }

    private void addConfigDefaults() {
        Map<String, Object> map = new HashMap<>();
        Location loc = getServer().getWorlds().get(0).getSpawnLocation();
        map.put("settings.tagTime", 20);
        map.put("settings.teleportTime", 5);
        map.put("settings.defaultPlayerBalance", 500);
        map.put("settings.map.schematic", "kitpvp");
        map.put("settings.map.name", "KitPvP");

        config.addDefaults(map);
        config.options().copyDefaults(true);
        try {
            config.save(configFile);
        } catch (IOException e) {
            l.w(e.getMessage());
        }
    }

    private void loadConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                l.w(e.getMessage());
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);

        if (!getDataFolder().exists()) getDataFolder().mkdir();
        dataFile = new File(getDataFolder(), "database.yml");
        if (!dataFile.exists()) {
            try {
                dataFile.createNewFile();
            } catch (IOException e) {
                l.w(e.getMessage());
            }
        }
        data = YamlConfiguration.loadConfiguration(dataFile);
    }

    public FileConfiguration getConfig() { return config; }

    public FileConfiguration getData() { return data; }

    public void addConfig(String path, Object value) {
        config.set(path, value);
        try {
            config.save(configFile);
        } catch (IOException e) {
            l.w(e.getMessage());
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }
    
    public void addData(String path, Object value) {
        data.set(path, value);
        try {
            data.save(dataFile);
        } catch (IOException e) {
            l.w(e.getMessage());
        }
    }

    public void registerListeners(Listener... listeners) {
        Arrays.stream(listeners).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));
    }

    private void registerCommands() {
        getCommand("kit").setExecutor(new KitCommand());
        getCommand("bank").setExecutor(new BankCommand());
        getCommand("balance").setExecutor(new BalanceCommand());
        getCommand("spawn").setExecutor(new SpawnCommand());
        getCommand("stats").setExecutor(new StatsCommand());
        getCommand("supplydrop").setExecutor(new SupplyDropCommand());
    }

    private void loadMap() {
        World world = mm.load();
        if (world == null) {
            throw new NullPointerException("the world cannot be null");
        }
    }

    public static Kitpvp get() {
        return instance;
    }

    public static String c(@Nullable String prefix, @NotNull String message) {
        return ChatColor.translateAlternateColorCodes('&', ((prefix!=null?("&2"+prefix+"&2>> &7"):("&7")))+message);
    }

    public static String c(@NotNull String message) {
        return c(null, message);
    }

    public static String cr(@NotNull String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public void broadcast(@NotNull String message) {
        UtilPlayer.forEach(p -> p.sendMessage(message));
        Bukkit.getLogger().info(message);
    }

    public static void startMessage(Player p) {
        p.sendMessage(cr("&3&m-------------------------------------------------"));
        p.sendMessage(cr("                           &b&lKitPvP"));
        p.sendMessage(cr("&r"));
        p.sendMessage(cr("&f    Kill other players, get supply drops and get more kits"));
        p.sendMessage(cr("&r"));
        p.sendMessage(cr("&b&l    Map by the RaptorPvP Build Team"));
        p.sendMessage(cr("&3&m-------------------------------------------------"));

    }
}
