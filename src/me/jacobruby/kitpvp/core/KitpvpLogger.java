package me.jacobruby.kitpvp.core;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

import static me.jacobruby.kitpvp.core.KitpvpLogger.Level.*;
import static org.bukkit.ChatColor.stripColor;

public class KitpvpLogger {

    private String prefix = "&9[RaptorKitPvP] ";

    public enum Level {
        INFO, WARN, SEVERE
    }

    public void i(String message) {
        log(message, INFO);
    }

    public void w(String message) {
        log(message, WARN);
    }

    public void s(String message) {
        log(message, SEVERE);
    }

    private void log(String message, Level i) {
        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        boolean valid = (console != null);
        if (i.equals(INFO)) {
            if (valid) {
                console.sendMessage(Kitpvp.c(null, prefix + message));
            } else {
                Bukkit.getLogger().info(stripColor(Kitpvp.c(null, prefix + message)));
            }
        } else if (i.equals(WARN)) {
            message = Kitpvp.c(null, prefix + "&6[WARNING] " + stripColor(message));
            if (valid) {
                console.sendMessage(message);
            } else {
                Bukkit.getLogger().warning(stripColor(message));
            }
        } else if (i.equals(SEVERE)) {
            message = Kitpvp.c(null, prefix + "&c[ERROR] " + stripColor(message));
            if (valid) {
                console.sendMessage(message);
            } else {
                Bukkit.getLogger().severe(stripColor(message));
            }
        }
    }

}
