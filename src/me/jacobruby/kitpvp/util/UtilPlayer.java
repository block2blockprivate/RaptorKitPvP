package me.jacobruby.kitpvp.util;

import org.bukkit.Bukkit;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class UtilPlayer {

    public static boolean isOnGround(Player p) {
        return UtilBlock.isSolid(p.getLocation().getBlock().getRelative(BlockFace.DOWN));
    }

    public static Set<Player> all() {
        Set<Player> set = new HashSet<>();
        set.addAll(Bukkit.getServer().getOnlinePlayers());
        return set;
    }

    public static void forEach(Consumer<? super Player> consumer) {
        for (Player p : all()) {
            consumer.accept(p);
        }
    }
}
