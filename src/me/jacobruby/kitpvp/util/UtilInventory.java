package me.jacobruby.kitpvp.util;

import me.jacobruby.kitpvp.mechanics.entities.Kit;
import me.jacobruby.kitpvp.mechanics.kits.PlayerKitType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


public class UtilInventory {

    public static void convertKit(Player p, PlayerKitType kit) {
        Kit k = kit.getKit();
        p.getInventory().clear();
        p.getEquipment().setArmorContents(new ItemStack[]{k.boots(), k.leggings(), k.chestplate(), k.helmet()});
        for (int i = 0; i<k.hb().length; i++) {
            ItemStack s = k.hb()[i];
            if (s != null) p.getInventory().setItem(i, s);
        }
        for (int i = 0; i<k.i().length; i++) {
            ItemStack s = k.i()[i];
            if (s != null) p.getInventory().setItem(i+9, s);
        }
    }

    public static void clear(Player p) {
        p.getInventory().clear();
        p.getInventory().setBoots(null);
        p.getInventory().setLeggings(null);
        p.getInventory().setChestplate(null);
        p.getInventory().setHelmet(null);
    }


}
