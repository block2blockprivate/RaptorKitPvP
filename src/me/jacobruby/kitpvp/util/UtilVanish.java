package me.jacobruby.kitpvp.util;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class UtilVanish {

    private static Kitpvp k = Kitpvp.get();

    public static boolean setInvisible(Player p, boolean visible) {
        if (visible == k.pm.specs.contains(p)) {
            return false;
        } else {
            if (visible) {
                k.pm.specs.add(p);
            } else {
                k.pm.specs.remove(p);
            }
            return true;
        }
    }

    public static void vanish(Player t) {
        t.setAllowFlight(true);
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            if (p!=t)
            p.hidePlayer(t);
        }
    }
    public static void unvanish(Player t) {
        if (!t.getGameMode().equals(GameMode.CREATIVE)) {
            t.setAllowFlight(false);
            t.setFlying(false);
        }
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            p.showPlayer(t);
        }
    }


}
