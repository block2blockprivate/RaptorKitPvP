package me.jacobruby.kitpvp.util;

import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.bukkit.block.BlockFace.*;

@SuppressWarnings("deprecation")
public class UtilBlock {

    public static List<Block> allDataInside(Block min, Block max) throws IllegalArgumentException {
        if (min.getWorld() != max.getWorld()) throw new
                IllegalArgumentException("thou shalt not put two blocks which are in different worlds");
        List<Block> list = new ArrayList<>();
        int x1 = min.getX();
        int y1 = min.getY();
        int z1 = min.getZ();
        int x2 = max.getX();
        int y2 = max.getY();
        int z2 = max.getZ();

        int a = x2-x1;
        int b = y2-y1;
        int c = z2-z1;

        for (int d=0; d<=a; d++) {
            for (int e=0; e<=b; e++) {
                for (int f=0; f<=c; f++) {
                    Block block = blockAt(min.getWorld(), x1+d, y1+e, z1+f);
                    if (block == null)
                        continue;
                    if (block.getType() != Material.WOOL)
                        continue;
                    if (block.getRelative(UP).getType() != Material.GOLD_PLATE)
                        continue;
                    list.add(block);
                }
            }
        }
        return list;
    }

    public static List<Block> allBlocksInside(Block min, Block max) throws IllegalArgumentException {
        if (min.getWorld() != max.getWorld()) throw new
                IllegalArgumentException("thou shalt not put two blocks which are in different worlds");
        String w = min.getWorld().getName();
        List<Block> list = new ArrayList<>();
        int x1 = min.getX();
        int y1 = min.getY();
        int z1 = min.getZ();
        int x2 = max.getX();
        int y2 = max.getY();
        int z2 = max.getZ();

        int a = x2-x1;
        int b = y2-y1;
        int c = z2-z1;

        for (int d=0; d<=a; d++) {
            for (int e=0; e<=b; e++) {
                for (int f=0; f<=c; f++) {
                    list.add(blockAt(w, x1+d, y1+e, z1+f));
                }
            }
        }
        return list;
    }

    public static boolean contains(List<Block> list, Location loc) {
        for (Block b : list) {
            if (loc.getBlock() == b) return true;
        }
        return false;
    }

    public static void clearMap(String schematic) throws IOException, DataException {
        File schem = new File("plugins/RaptorKitPvP/schematics/"+schematic+".schematic");
        if (!schem.exists()) {
            throw new FileNotFoundException("file "+schematic+" was not found in plugins\\RaptorKitPvP\\schematics");
        }

        int length = MCEditSchematicFormat.getFormat(schem).load(schem).getLength();
        int width = MCEditSchematicFormat.getFormat(schem).load(schem).getWidth();
        int height = MCEditSchematicFormat.getFormat(schem).load(schem).getHeight();
        for (Block b : allBlocksInside(blockAt(UtilLocation.defaultWorld(), 0, 50, 0), blockAt(UtilLocation.defaultWorld(), length+1, height+51, width+1))) {
            if (b.getType().equals(Material.AIR)) continue;
            b.setType(Material.AIR);
        }
    }

    public static Block blockAt(World world, int x, int y, int z) {
        return world.getBlockAt(x, y, z);
    }

    public static Block blockAt(String world, int x, int y, int z) {
        return Bukkit.getServer().getWorld(world).getBlockAt(x, y, z);
    }

    public static boolean isDataPoint(Block block) {
        return block.getRelative(UP).getType().equals(Material.GOLD_PLATE);
    }

    public static boolean isSolid(Block b) {
        switch (b.getType()) {
            case AIR:
            case RAILS:
            case ACTIVATOR_RAIL:
            case DETECTOR_RAIL:
            case POWERED_RAIL:
            case SUGAR_CANE_BLOCK:
            case FLOWER_POT:
            case YELLOW_FLOWER:
            case RED_ROSE:
            case DOUBLE_PLANT:
            case GOLD_PLATE:
            case IRON_PLATE:
            case WATER:
            case WATER_LILY:
            case TORCH:
            case REDSTONE_TORCH_OFF:
            case REDSTONE_TORCH_ON:
            case STATIONARY_WATER:
            case LAVA:
            case STATIONARY_LAVA:
            case SIGN_POST:
            case WALL_SIGN:
            case FIRE:
            case WEB:
            case SAPLING:
            case VINE:
            case DEAD_BUSH:
            case STONE_BUTTON:
            case WOOD_BUTTON:
            case LEVER:
            case REDSTONE_COMPARATOR_OFF:
            case REDSTONE_COMPARATOR_ON:
            case REDSTONE_WIRE:
            case LONG_GRASS:
            case BROWN_MUSHROOM:
            case RED_MUSHROOM:
            case CROPS:
            case STONE_PLATE:
            case WOOD_PLATE:
            case CAKE_BLOCK:
            case TRAP_DOOR:
            case MELON_STEM:
            case PUMPKIN_STEM:
            case ACACIA_FENCE_GATE:
            case BIRCH_FENCE_GATE:
            case DARK_OAK_FENCE_GATE:
            case FENCE_GATE:
            case JUNGLE_FENCE_GATE:
            case SPRUCE_FENCE_GATE:
            case NETHER_WARTS:
            case BREWING_STAND:
            case PORTAL:
            case ENDER_PORTAL:
            case DRAGON_EGG:
            case COCOA:
            case TRIPWIRE_HOOK:
            case TRIPWIRE:
            case CARROT:
            case POTATO:
            case SKULL:
            case DAYLIGHT_DETECTOR:
            case DAYLIGHT_DETECTOR_INVERTED:
            case IRON_TRAPDOOR:
            case CARPET:
            case BANNER:
            case STANDING_BANNER:
            case WALL_BANNER: return false;
            default: return true;
        }
    }

    public static Location toLocation(Block b) {
        return b.getLocation().add(0.5, 0, 0.5);
    }

    public static Location toLocationMiddle(Block b) {
        return b.getLocation().add(0.5, 0.5, 0.5);
    }
}
