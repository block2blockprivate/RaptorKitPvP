package me.jacobruby.kitpvp.util;

import me.jacobruby.kitpvp.core.Kitpvp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

public class UtilLocation {

    private static Kitpvp k = Kitpvp.get();

    public static String serialize(Location loc) {
        return loc.getWorld().getName()+", "+loc.getBlockX()+", "+loc.getBlockY()+", "+loc.getBlockZ()+
                ", "+loc.getYaw()+", "+loc.getPitch();
    }

    public static Location deserialize(String loc) {
        String[] args = loc.split(", ");
        try {
            return new Location(Bukkit.getServer().getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]),
                    Float.parseFloat(args[4]), Float.parseFloat(args[5]));
        } catch (Exception e) {
            k.l.s("Location " + loc + " failed to deserialize");
            return null;
        }
    }

    public static boolean isInSpawn(Location loc) {
        return loc.getBlockY()>k.mm.spawnYLevel;
    }

    public static World defaultWorld() {
        return Bukkit.getServer().getWorld(k.getConfig().getString("settings.map.name"));
    }

    public static Set<Vector> line(Vector a, Vector b, double gap) {
        Set<Vector> vectors = new HashSet<>();
        double distance = a.distance(b) - 1;
        int points = (int) (distance / gap) + 1;
        double vgap = distance/(points-1);
        Vector c = b.clone().subtract(a).normalize().multiply(vgap);
        for (int i = 1; i<points; i++) {
            vectors.add(a.clone().add(c.clone().multiply(i)));
        }
        return vectors;
    }

    public static Set<Vector> line(Vector a, Vector b) {
        return line(a, b, 0.3D);
    }
}
