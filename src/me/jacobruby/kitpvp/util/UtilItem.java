package me.jacobruby.kitpvp.util;

import me.jacobruby.kitpvp.core.Kitpvp;
import me.jacobruby.kitpvp.mechanics.entities.Enchant;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import java.util.Arrays;

import static org.bukkit.Material.*;

public class UtilItem {

    private static Kitpvp k = Kitpvp.get();

    public static void giveKill(Player p) {
        p.getInventory().addItem(Bank.gapple(k.pm.getPlayerKit(p).type.getName()));
    }

    public static ItemStack ci(Material type, String name, int amount, String lore, short data, String skullName, Enchant... enchants) {
        ItemStack is = new ItemStack(type, amount, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(Kitpvp.cr(name));
        if (lore != null) {
            im.setLore(Arrays.asList(Kitpvp.c(lore).split(",")));
        }
        if (skullName != null) {
            SkullMeta sm = (SkullMeta) im;
            sm.setOwner(skullName);
            im = sm;
        }
        for (Enchant e : enchants) {
            im.addEnchant(e.e(), e.l(), true);
        }
        im.spigot().setUnbreakable(true);
        im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ATTRIBUTES);
        is.setItemMeta(im);
        return is;
    }
    public static ItemStack ci(Material type, String name, int amount, String lore, short data, Enchant... enchants) {
        return ci(type, name, amount, lore, data, null, enchants);
    }
    public static ItemStack ci(Material type, String name, int amount, String lore, Enchant... enchants) {
        return ci(type, name, amount, lore, (short)0, enchants);
    }
    public static ItemStack ci(Material type, String name, int amount, Enchant... enchants) {
        return ci(type, name, amount, null, enchants);
    }
    public static ItemStack ci(Material type, String name, Enchant... enchants) {
        return ci(type, name, 1, enchants);
    }
    public static ItemStack ci(Material type, Enchant... enchants) {
        return ci(type, "", enchants);
    }

    public static ItemStack potion(String name, int amount, String lore, PotionEffect... effects) {
        ItemStack p = ci(POTION, name, amount, lore);
        PotionMeta pm = (PotionMeta) p.getItemMeta();
        for (PotionEffect e : effects) {
            pm.addCustomEffect(e, true);
        }
        p.setItemMeta(pm);
        return p;
    }

    public static class Bank {
        public static ItemStack gapple(String kit) {
            ItemStack is = new ItemStack(GOLDEN_APPLE);
            ItemMeta im = is.getItemMeta();
            im.setDisplayName(Kitpvp.cr("&e" + kit + "&e's Apple"));
            im.setLore(Arrays.asList(Kitpvp.cr("&7This apple is capable of healing,&7anyone who consumes it.," +
                    "&7It is given as a reward,&7for slaying a player.").split(",")));
            is.setItemMeta(im);
            return is;
        }
        public static ItemStack opgapple(String kit) {
            ItemStack is = new ItemStack(GOLDEN_APPLE, 1, (short)1);
            ItemMeta im = is.getItemMeta();
            im.setDisplayName(Kitpvp.cr("&e" + kit + "&e's Apple"));
            im.setLore(Arrays.asList(Kitpvp.cr("&7This apple is capable of healing,&7anyone who consumes it.").split(",")));

            is.setItemMeta(im);
            return is;
        }
        public static ItemStack cleaver() {
            return UtilItem.ci(DIAMOND_AXE, "&9Cleaver", 1, "" +
                    "The mighty Cleaver is capable," +
                    "&7of crippling it's victims and," +
                    "&7harnessing the power of wind," +
                    "&7to send its user fowards at," +
                    "&7very high speeds.," +
                    "&aRight click to use," +
                    "&6&lCharge", new Enchant(Enchantment.DAMAGE_ALL, 1));
        }
        public static ItemStack storm() {
            ItemStack i = ci(BOW, "&bStorm", 1, "" +
                    "This legendary bow has the," +
                    "&7souls of a over 9 thousand," +
                    "&7storm clouds trapped inside," +
                    "&7of it. Every shot is infused with," +
                    "&7the power of a lightning bolt.," +
                    "&aLeft click to prepare," +
                    "&b&lElectric Arrow", new Enchant(Enchantment.ARROW_INFINITE, 1), new Enchant(Enchantment.ARROW_FIRE, 1), new Enchant(Enchantment.ARROW_KNOCKBACK, 1));
            ItemMeta im = i.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            i.setItemMeta(im);
            return i;
        }
        public static ItemStack sniper() {
            ItemStack i = ci(FEATHER, "&bThe Feather of Invisibility", 1, "" +
                    "This legendary feather has the," +
                    "&7ability to turn the bearer invisible," +
                    "&7and allow them to sneak around until," +
                    "&7they go in for the strike!," +
                    "&aLeft click to become," +
                    "&ainvisible!");
            ItemMeta im = i.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            i.setItemMeta(im);
            return i;
        }

        public static ItemStack slowness() {
            ItemStack i = ci(NETHER_STAR, "&bThe Star of Slowness", 1, "" +
                    "This legendary star has the," +
                    "&7ability to slow all opponents," +
                    "&7and allows the bearer an advantage," +
                    "&7in any situation!," +
                    "&aLeft click to enable," +
                    "&aslowness!", new Enchant(Enchantment.ARROW_INFINITE, 1), new Enchant(Enchantment.ARROW_DAMAGE, 3), new Enchant(Enchantment.ARROW_KNOCKBACK, 1));
            ItemMeta im = i.getItemMeta();
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            i.setItemMeta(im);
            return i;
        }
    }

}
